#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
Return fault index for the target data file, which consist of faulty method ids and all method feature datas.
If some of the fault methods are not find, print error message and current found items 
"""
def find_faulty_index(dataset, data_name):
	fault_index = []
	(faulty_method_list, vectors) = dataset; count = 0; num_faults = len(faulty_method_list)
	trace_list = dict()
	for f in faulty_method_list:
		trace_list[f] = 0

	for i in range(len(vectors)):	
		if (vectors[i][0] in faulty_method_list):
			fault_index.append(i); count += 1
			trace_list[vectors[i][0]] = 1
		if (count == num_faults):
			break

	if (count != num_faults):
		print "have some remains " + data_name
		f_list = trace_list.items()
		for item in f_list:
			if (int(item[1]) == 0):
				print item[0]
	return fault_index
