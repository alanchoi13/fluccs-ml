#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

import csv
import os
import re

script_dir = os.path.dirname(os.path.abspath(__file__))

def from_which(project, bug_ver, class_name, line_number):
	global script_dir
	project = str(project); bug_ver = str(bug_ver); class_name = str(class_name); line_number = str(line_number) 
	loc = os.path.join(os.path.abspath(os.path.join(script_dir, os.pardir)), "method_stmt/" + project)
	handler = open(os.path.join(loc, "stmt_mth_" + project + "_" + bug_ver+".csv"), "r")
	csvReader = csv.reader(handler, delimiter = ",")
	datas = list(tuple(row) for row in csvReader)
	
	from_method = "none"
	from_sub_class = "none"
	class_loc = class_name.replace(".","/")
	find = 0

	for data in datas:
		if (data[0] == class_name and data[1] == line_number):
			from_infos = data[2]
			from_sub_class = None
			from_method_infos = re.search("(.*)\<.*\>", from_infos).group(1)
			
			if (class_loc in from_method_infos):
				from_sub_class = re.search(re.escape(class_loc)+".*\$([^\$]+)\$[^\$]+\<", from_infos).group(1)
				from_method = re.search("\$([^\$]+\<.*\>)", from_infos).group(1)
				#print("statement " + str(line_number) + " in " + class_name + " is in class "+from_sub_class+ " and method " + from_method + "\n") # logging
				find = 1
				return [from_sub_class, from_method]
			else:
				from_method = from_infos
				#print("statement " + str(line_number) + " in " + class_name + " is in method " + from_method + "\n") # logging
				find = 1
				return [from_method,]
	if (find == 0):
		#print (class_name + " and " + str(line_number) + " is not an useful statement thus no info for this statement\n") # logging
		return None
