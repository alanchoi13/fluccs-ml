#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
class for code and change metrics: age, churn, complexity.
"""
import csv
import sys
import os
import re
import Method_age
import Method_churn
import Method_complexity
import targetMethod

#class for call graph
class CG_Metrics:
	def __init__(self, loc, pid, bid, which, datadir = None):
		self.methods = {}
		self.targetMethodInfoHandler = targetMethod.TargetMethodInfo(pid, bid)	

		if (which == "age"):
			self._construct_age(loc, pid, bid)
		elif (which == "churn"):
			self._construct_churn(loc, pid, bid, datadir)
		elif (which == "complexity"):
			self._construct_complexity(loc, pid, bid)
		else:
			print "wrong metric type request " + which + "\n"
			sys.exit()

	#construct call graph for the project
	def _construct_age(self, method_calls_dir, pid, bid):
		datas = self._get_datas(os.path.join(method_calls_dir, pid + "/" + bid + "/java_sig_method_call.csv"))
		init_datas = self._get_datas(os.path.join(method_calls_dir, pid + "/" + bid + "/java_sig_class_methods.csv"))
	
		#initialize methods 	
		for init_data in init_datas:
			arg = init_data[2]
			if ("$1;" in init_data[2]):
				arg = self._get_arg(init_data, 2, 1)
			self.methods[init_data[0] + "$" + init_data[1] + arg] = Method_age.Method(init_data[0], init_data[1] + arg)
	
		for data in datas:#dealing with method_call data
			class_name = data[0]
			method_name = data[1] + data[2]
			
			if ("$1;" in data[2]):
				arg = self._get_arg(data, 2, 0)
				method_name = data[1] + arg

			calls_len = len(data)
			for i in range(3, calls_len, 3):
				call = data[i] + "$" + data[i + 1] + data[i + 2]
				if ("$1;" in data[i + 2]):
					arg = self._get_arg(data, i + 2, 0)
					call = data[i] + "$" + data[i + 1] + arg
				self.methods[class_name + "$" + method_name]._add_(self.methods[call])
	
	#Construct data structure for churn
	def _construct_churn(self, method_calls_dir, pid, bid, datadir):
		churn_datas = self._get_datas(os.path.join(datadir,"churn_method.csv"))
		datas = self._get_datas(os.path.join(method_calls_dir, pid + "/" + bid + "/java_sig_method_call.csv"))
		for churn_data in churn_datas:
			self.methods[churn_data[0]] = Method_churn.Method(churn_data[0], churn_data[1])
		
		for data in datas:
			class_name = data[0]
			method_name = data[1] + data[2]
			calls_len = len(data)
			if (class_name + "$" + method_name in self.methods.keys()):
				for i in range(3, calls_len, 3):
					call = data[i] + "$" + data[i + 1] + data[i + 2]
					if ("$1;" in data[i + 2]):
						arg = self._get_arg(data, i + 1, 0)
						call = data[i] + "$" + data[i + 1] + arg
					if (call in self.methods.keys()):
						self.methods[class_name + "$" + method_name]._add_(self.methods[call])


	#Construct data structure for complexity
	def _construct_complexity(self, method_calls_dir, pid, bid):
		init_datas = self._get_datas(os.path.join(method_calls_dir, pid + "/" + bid + "/java_sig_class_methods.csv"))

		for init_data in init_datas:
			arg = init_data[2]
			methodId = init_data[0] + "$" + init_data[1] + arg
			if (self.targetMethodInfoHandler.isTarget(methodId)):
				#num_of_args, num_of_local_vars, num_instr_length
				self.methods[methodId] = Method_complexity.Method(init_data[0], init_data[1] + arg, init_data[3], init_data[4], init_data[5])
			else:				
				#argument parsing
				if ("$1;" in init_data[2]):
					arg = self._get_arg(init_data, 2, 1)
			
					methodId = init_data[0] + "$" + init_data[1] + arg
					if (self.targetMethodInfoHandler.isTarget(methodId)):
						self.methods[methodId] = Method_complexity.Method(init_data[0], init_data[1] + arg, init_data[3], init_data[4], init_data[5])
				
			

	#return argument for the target : data[index]	
	def _get_arg(self, data, index, deep):
		arg = None
		if (data[index].count(";") > 1):
			match = re.search("(.*);[^\;]+\$1\;\>", data[index])
			if (deep and match == None):
				match = re.search("\$1\;(.*)\>", data[index])
				arg = "<" + match.group(1) + ">"
			else:
				arg = match.group(1) + ";>"
		else:
			arg = "<>"
		return arg


	#retrieve data from filename
	def _get_datas(self, filename):
		handler = open(filename)
		csvReader = csv.reader(handler, delimiter = ",")
		return_datas = list(tuple(row) for row in csvReader)
		handler.close()
		return return_datas

	
	#check whether current method, which is identified by methodId, is in the target #method list
#	def _isTarget(self, methodId, pid, bid):
#		handler = open(os.path.join(os.environ['D4J_HOME'], "framework/bin/fluccs/method_stmt/" + pid + "/Methods/" + pid + "." + bid + ".csv"))
#		csvReader = csv.reader(handler, delimiter = ",")
#		#row[0] is the key identifier for each target method
#		targets = [row[0] for row in csvReader]
#		handler.close()
#		if (methodId in targets):
#			return True
#		else:
#			return False

	def _print_out(self):
		for method_name in self.methods.keys():
			#print "key of methods " + method_name # loggng
			#print "method " + self.methods[method_name].class_in + " " + self.methods[method_name].name + " calls " + str(len(self.methods[method_name].calls)) # logging
			for call in self.methods[method_name].calls:
				print call.class_in + "," + call.name + ";" 
				


