import os, sys
import fnmatch
import re
import argparse
import subprocess

#find the source directory using command : ant get-source-home and parsing the output
def find_source_dir(workdir):
	source_dir = None
	cmd = "ant get-source-home"
	proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	print std_out
	print std_err
	if (not (not std_err)): print std_err; sys.exit()
	std_out_lines = std_out.split("\n");
	for std_out_line in std_out_lines:
		if ('[echo]' in std_out_line):
			matched = re.match('\s+\[echo\]\s+(.*)', std_out_line)
			if (matched is not None):
				source_dir = matched.group(1)
			else:
				print "Error while finding source home: Cannot find matched source home"
				sys.exit()
			break
	return source_dir

#find classes in source and write them out on classes.list
def find_classes(source_dir, workdir):
	matches = list()
	#print source_dir #--> logging
	for root, dirnames, filenames in os.walk(source_dir):
		for filename in fnmatch.filter(filenames, "*.java"):
			filename = os.path.join(root,filename).replace("/", ".")
			filename = filename[len(source_dir) + 1:-5];
			matches.append(filename)

	#print matches #--> logging
	with open(os.path.join(workdir, "classes.list"), "w") as handler:
		for filename in matches:
			handler.write(filename + "\n")


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-workdir", action = "store", default = None);
	args = parser.parse_args()
	workdir = args.workdir;
	os.chdir(workdir)
	source_dir = find_source_dir(workdir)
	find_classes(source_dir, workdir)
