import os, sys
import csv

class TargetMethodInfo:
	def __init__(self, pid, bid):
		self.targetMethod = list()
		filePath = os.path.join(os.environ["D4J_HOME"], "framework/bin/fluccs/method_stmt/" \
			+ pid + "/Methods/" + pid + "." + bid + ".csv")

		csvReader = csv.reader(open(filePath))
		for row in csvReader:
			#methodID = temp[0], fileLoc = temp[1], className = temp[2], methodName = temp[3], args = temp[4:]
			methodID = row[0]
			self.targetMethod.append(methodID)	

	#read target method file adn return the data
	def getTargetMethods(self):
		return this.targetMethod	

	def isTarget(self, methodId):
		if (methodId in self.targetMethod):
			return True
		else:
			return False
