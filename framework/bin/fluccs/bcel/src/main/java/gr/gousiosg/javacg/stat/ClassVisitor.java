/*
 * Copyright (c) 2011 - Georgios Gousios <gousiosg@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gr.gousiosg.javacg.stat;

import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.EmptyVisitor;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.generic.ConstantPoolGen;
import org.apache.bcel.generic.MethodGen;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.apache.bcel.generic.Type;

import java.lang.reflect.*;
import java.lang.Class;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.util.ClassPath;
import org.apache.bcel.classfile.LocalVariableTable;
import org.apache.bcel.generic.InstructionList;

/**
 * The simplest of class visitors, invokes the method visitor class for each
 * method found.
 */
public class ClassVisitor extends EmptyVisitor {

    private JavaClass clazz;
    private ConstantPoolGen constants;
    private String classReferenceFormat;
    
    public ArrayList<String> global_class_list; //use instead of global_method_list
    public ArrayList<String> global_method_list;
    private MyClassLoader my_loader;
    private String prefix;
	////
    private String zipfile;

    public ClassVisitor(JavaClass jc) {
        clazz = jc;
        global_method_list = new ArrayList<String>();
        constants = new ConstantPoolGen(clazz.getConstantPool());
    }

    public ClassVisitor(JavaClass jc, ArrayList<String> global_class_list, ArrayList<String> global_method_list, 
        MyClassLoader my_loader, String prefix, String zipfile) {
        clazz = jc;
        this.global_class_list = global_class_list;
        this.global_method_list = global_method_list;
        this.my_loader = my_loader;
        this.prefix = prefix;
        ////
        this.zipfile = zipfile;
        constants = new ConstantPoolGen(clazz.getConstantPool());
        classReferenceFormat = "C:" + clazz.getClassName() + " %s";
    }

    public void visitJavaClass(JavaClass jc) {
        jc.getConstantPool().accept(this);
        Method[] methods = jc.getMethods();
        for (int i = 0; i < methods.length; i++){
            methods[i].accept(this);
        }
    }

    
    //To create this class part of global_method_list
    public void create_method_list(JavaClass jc) {
        jc.getConstantPool().accept(this);//call visitConstantPool
        Method[] methods = jc.getMethods();
        ArrayList<String> class_parent = new ArrayList<String>();
        JavaClass[] parents = null;
        try{
            FileWriter fw = new FileWriter("output/class_methods.csv", true);
            BufferedWriter bw = new BufferedWriter(fw);

            for (Method method : methods){
                if (!method.getName().contains("access$")){
            	    String arguments = "<";
            	    int count = 1;

                    for (Type type : method.getArgumentTypes()){
                        if (count == method.getArgumentTypes().length){
                    		arguments += type.toString();
                	    }
                	    else{
                    		arguments += type.toString() + ";";
                    		count ++;
                	   }
                    }
                    arguments += ">";

                    global_method_list.add(jc.getClassName() + "$" + method.getName() + "$" + arguments);
                    int num_of_local_vars = 0;
                    LocalVariableTable local_vars = method.getLocalVariableTable();
        
                    MethodGen mg = new MethodGen(method, jc.getClassName(), constants);
                    InstructionList instr_list = mg.getInstructionList();
                    int instr_list_length = 0;

                    if (instr_list != null){
                        instr_list_length = instr_list.getLength();
                    }

                    if (local_vars != null){
                        num_of_local_vars = local_vars.getTableLength();
                    }
                    else{
                        num_of_local_vars = 0;
                    }

                    bw.write(jc.getClassName() + "," + method.getName() + "," + arguments + "," 
                        + Integer.toString(method.getArgumentTypes().length) 
                        + "," + Integer.toString(num_of_local_vars) + "," + Integer.toString(instr_list_length));

                    bw.newLine();
                    bw.flush();
                }
            }
            bw.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

	//get constant pool call this method
    public void visitConstantPool(ConstantPool constantPool) {
    }

    public void visitMethod(Method method) {
        if (!method.getName().contains("access$")){
        	MethodGen mg = new MethodGen(method, clazz.getClassName(), constants);
        	MethodVisitor visitor = new MethodVisitor(mg, clazz, global_class_list, global_method_list, my_loader, prefix, zipfile);
        	visitor.start(); 
        }
    }

    public void start() {
        visitJavaClass(clazz);
    }

    public void pre_process(){
        create_method_list(clazz);
    }
}
