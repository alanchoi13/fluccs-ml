#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
import re

class Convert_to_java_sig:
	def __init__(self):
		self.java_sig_list = ["boolean", "byte", "char", "short", "int", "long", "float", "double", "void"]
		self.java_sig = dict()
		self.java_sig["boolean"] = "Z"
		self.java_sig["byte"] = "B"
		self.java_sig["char"] = "C"
		self.java_sig["short"] = "S"
		self.java_sig["int"] = "I"
		self.java_sig["long"] = "J"
		self.java_sig["float"] = "F"
		self.java_sig["double"] = "D"
		self.java_sig["void"] = "V"
		self.java_sig["fully"] = "L"

	def convert(self, targets):
		sig = ""
		sigs = []
		converted_sigs = ""
		array_pattern ="[]"
		targets = re.search("\<(.*)\>", targets).group(1)
		target_list = targets.split(";")
		for target in target_list:
			if (re.search(".*\[\]", target)):
				array_type = re.search("([^\[\]]*)\[\]", target).group(1)
				array_part = re.search("[^\[\]]+(.*)", target).group(1)

				if (array_type in self.java_sig_list):
					sig = self.java_sig[array_type]
				else:
					array_type = array_type.replace(".", "/")
					sig = self.java_sig["fully"] + array_type + ";"					
		
				sig_array_part = ""
				while(re.search("\[\]", array_part)):
					array_part = re.search("^\[\](.*)", array_part).group(1)
					sig_array_part += "["
				sig = sig_array_part + sig
		
			else:
				if (target in self.java_sig_list):	
					sig = self.java_sig[target]
				else:
					if (target == ""):
						sig = ""
					else:
						target = target.replace(".", "/")
						sig = self.java_sig["fully"] + target + ";"
			
			sigs.append(sig)
		
		converted_sigs = "<" + "".join(sigs) + ">"
		return converted_sigs

