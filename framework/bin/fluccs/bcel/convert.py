#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

import sys
import os
import csv
import Convert_to_java_sig
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import parser_check

def convert(loc, target_file, class_file):
	handler = open(os.path.join(loc, target_file), "r")	
	csvReader = csv.reader(handler, delimiter = ",")
	datas = list(list(row) for row in csvReader)
	handler.close()
	converted_datas = []
	
	converter = Convert_to_java_sig.Convert_to_java_sig() 

	if (class_file):	
		for data in datas:
			java_sig = converter.convert(data[2])
			data[2] = java_sig
	else:
		for data in datas:
			data_len = len(data)
			for i in range(2, data_len, 3):
				java_sig = converter.convert(data[i])
				data[i] = java_sig
	
					
	handler = open(os.path.join(loc, "java_sig_"+target_file), "w")
	csvWriter = csv.writer(handler, delimiter = ",", quotechar = "|")
	for data in datas:
		csvWriter.writerow(data)
	
	handler.close()

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-filename", action = "store", default = None)
	parser.add_argument("-isclass", action = "store", type = int, default = 0)
	args = parser.parse_args()
	parser_check.argument_check(parser, args_name_list = ["datadir", "filename"], args_val_list = [args.datadir, args.filename])
	convert(args.datadir, args.filename, args.isclass)
