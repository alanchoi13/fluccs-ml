import sys
import os, csv
import argparse

pythonLibDir = os.path.join(os.environ['D4J_HOME'], "framework/bin/fluccs/python")
sys.path.insert(0, pythonLibDir)

import Which_from
import parser_check

def gatherMethodAndLines(pid, bid):
	method_stmtDir = os.path.join(os.environ['D4J_HOME'], "framework/bin/fluccs/method_stmt/" + pid)
	method_stmtFile = os.path.join(method_stmtDir, "stmt_mth_" + pid + "_" + bid + ".csv")
	csvReader = csv.reader(open(method_stmtFile))
	datas = list(tuple(row) for row in csvReader)

	csvWriter = csv.writer(open(os.path.join(method_stmtDir, "stmt_mth_id_" + pid + "_" + bid + ".csv"), "w"))	
	methodAndLines = dict()
	for data in datas:
		class_name = data[0]; line_number = data[1]; from_infos = data[2]
		origin = Which_from.from_which(class_name, line_number, from_infos)
		if (origin == None):
			print "invalid statement... error in stmt_method file"
			sys.exit()
		else:
			if (len(origin) == 2):#subclass, methodname<args>
				cand = class_name + "$" + origin[0] + "$" + origin[1]
			elif (len(origin) == 1):#methodname<args>
				cand = class_name + "$" + origin[0]
			else:
				print "invalid number of origin infos... error in stmt_method file"
				print origin
				sys.exit()

		csvWriter.writerow([class_name, line_number, cand])


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-pid", action = "store", default = None)
	parser.add_argument("-bid", action = "store", default = None)
	args = parser.parse_args()

	parser_check.argument_check(parser, args_name_list = ["pid", "bid"], args_val_list = [args.pid, args.bid])
	gatherMethodAndLines(args.pid, args.bid)
		 			
