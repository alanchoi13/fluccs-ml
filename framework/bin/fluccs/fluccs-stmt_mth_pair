#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

use strict;
use warnings;
use File::Spec;
use Getopt::Std;
use Cwd;
use lib "$ENV{'D4J_HOME'}/framework/bin/fluccs";
use perl::project;

my %cmd_opts;
getopts('p:b:w:c:', \%cmd_opts);

my $pid = $cmd_opts{p};
my $bid = $cmd_opts{b};
my $temp_w = $cmd_opts{w};
my $use_cobertura = $cmd_opts{c};

if ($temp_w =~ /\/$/){
        $temp_w = $`;
}
my $workdir = abs_path($temp_w);
my $gen_stmt_mth_dir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/gen_stmt_mth_pair");
my $mth_stmt_dir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/method_stmt");
my $extractTargetDir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/CodeAndChangeM");
my $findBugDir = File::Spec->catfile($ENV{'D4J_HOME'}, "framework/bin/fluccs/fault_list");

my $cmd = "perl $gen_stmt_mth_dir/random_test.pl -p $pid -w $workdir -c $use_cobertura";
_execute($cmd);

$cmd = "perl $gen_stmt_mth_dir/gen_stmt_mth_pair.pl -p $pid -b $bid -w $workdir -r $mth_stmt_dir -c $use_cobertura";
_execute($cmd);

$cmd = "python $gen_stmt_mth_dir/convertToCommon.py -pid $pid -bid $bid";
_execute($cmd);

my $current_dir = getcwd();
chdir($workdir);

my $srcdir = srcdir_from_ant($pid);

chdir($extractTargetDir);
_execute($cmd);
#$1 = working directory, $2 = src directory $3 = project id, $4 = bug number
if (index($srcdir, $workdir) == -1){
	$srcdir = File::Spec->catfile($workdir, $srcdir);
}
$cmd = "$extractTargetDir/extractTargets.sh $workdir $srcdir $pid $bid";
_execute($cmd);

chdir($current_dir);

#find the locations of the buggy method for this fault
#found buggy methods will be recorded under $findBugDir/$pid
$cmd = "python $findBugDir/find_buggy_methods.py -workdir $workdir -pid $pid -bid $bid";
_execute($cmd);


sub _execute{
	my $cmd = shift @_;
	print $cmd."\n";
        my $out = qx($cmd 2>&1);
	print $out;
        my $ret = $?;
        if ($ret == -1){ die("failed to execute $cmd $out"); }

}

1;
