
use File::Spec;
use Try::Tiny;

#write target.properties for next testing
sub write_target{
        @_ == 3 or die $ARG_ERROR;
        my ($base_dir, $test_class, $test_method) = @_;
        my $handler;
        my $target=File::Spec->catfile($base_dir, "target.properties");

        print "writing target $test_class $test_method\n";

        if( -e $target){
                open($handler,">","$target") or die "cannot open the file $target: $!";
                close($handler) or warn "close file failed : $!";
        }

        open($handler, ">","$target") or die "cannot open the file $target: $!";
        try {
                print $handler "test.entry.class=$test_class\n";
                print $handler "test.entry.method=$test_method\n";
                print $handler "class=false\n";
                print $handler "all=false";

        }catch{
                warn "caught error : $_";
        };

        close($handler) or warn "close file failed : $!";
        return 1;
}

1;
