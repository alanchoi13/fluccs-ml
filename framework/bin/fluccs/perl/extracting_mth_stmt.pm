
use strict;
use warnings;
use XML::LibXML;
use List::MoreUtils 'any';

sub extract_mth_stmt_cobertura{
	my ($coverage_file, @classes_list) = @_;
	my $file_name = undef;
	my $package_name = undef;
	my $parser = XML::LibXML->new(load_ext_dtd => 0);
	my $doc = $parser->parse_file($coverage_file);
	my $xc = XML::LibXML::XPathContext->new($doc);
	my %line_infos_hash = ();
	
	my @java_sigs = ("Z","B","C","S","I","J","F","D","V");
	my @m_list = undef;

	#class format: i.e. org.A.B.
	for my $class (@classes_list){
		$class =~ /(.*)\.[^\.]+/;
		$package_name = $1;
		$file_name = $class;
		$file_name =~ tr/\./\//;
		
		my @stmt_infos = ();
		my @c_nodes = $xc->findnodes('/coverage/packages/package[@name="'.$package_name.'"]/classes/class[@filename="'."$file_name.java".'"]');
		my @visited_lines = ();

		my @sorted_c_nodes = sort {length($b->getAttribute('name')) <=> length($a->getAttribute('name'))} @c_nodes;
		for my $c_node (@sorted_c_nodes){
			my $class_name = $c_node->getAttribute('name');
			$class_name =~ tr/\./\//;
			my $prefix = "";
			if (length($class_name) > length($file_name)){# and $class_name =~ /$file_name\$/){
				$prefix = $class_name;
			}
			my @m_nodes = $c_node->findnodes('./methods/method');
			for my $m_node (@m_nodes){
				my $m_name = $m_node->getAttribute('name');
				my $args = $m_node->getAttribute('signature');
				$args =~ /\((.*)\)/;
				$args = $1;
				if (length($prefix)){
					$m_name=$prefix.'$'.$m_name;
				}
				$m_name = "$m_name<$args>";
				my @l_nodes = $m_node->findnodes('./lines/line');
				for my $l_node (@l_nodes){
					my $line_number = $l_node->getAttribute('number');
					if (! any {$_ == $line_number} @visited_lines){
						push @visited_lines, $line_number;
						#print "$line_number,$m_name\n";
						push @stmt_infos, "$line_number,$m_name";
					}
				}
			}
		}
		$line_infos_hash{$class} = \@stmt_infos;

	}

	return %line_infos_hash;	
}



#same with extract_line_cobertura except using jacoco, and thus different coverage file format
sub extract_mth_stmt{
	my ($coverage_file,@classes_list)=@_;
	my $file_name=undef;
	my $package_name=undef;
	my $file_name_wo_dir=undef;
	my $parser=XML::LibXML->new(load_ext_dtd =>0);
	my $doc = $parser->parse_file($coverage_file);
	my $xc=XML::LibXML::XPathContext->new($doc);
	my %line_infos_hash=();
	my $index = 0;
	my $first_mth_line_num = 0;

	my @java_sigs = ("Z","B","C","S","I","J","F","D","V");		
	my @m_list = undef;

	for my $class (@classes_list){
		$file_name=$class;
		$file_name=~ s/\./\//g;
		$file_name=~ /(.*)\/([^\/]+)/;
		$package_name=$1;
		$file_name_wo_dir="$2.java";
		
		my @m_nodes = $xc->findnodes('/report/package[@name="'.$package_name.'"]/class[@name="'.$file_name.'"]/method'); 	
		my @cand_subclass_nodes = $xc->findnodes('/report/package[@name="'.$package_name.'"]/class');
		#subclass name list of file_name class 
		my @subclasses = find_subclasses($file_name, @cand_subclass_nodes);
		my %subclass_m_nodes = ();
		#find method nodes of subclass with name attribute subclass
		foreach my $subclass (@subclasses){
			my @node_array = $xc->findnodes('/report/package[@name="'.$package_name.'"]/class[@name="'.$subclass.'"]/method');
			$subclass_m_nodes{$subclass} = \@node_array;
		} 
	
		my @st_nodes = $xc->findnodes('/report/package[@name="'.$package_name.'"]/sourcefile[@name="'.$file_name_wo_dir.'"]/line');

		my @stmt_infos=();
		my @mth_infos=();
		my @covered_lines = ();	

		@m_list = ();
		#create method infos. method information is sorted under the start point of the mehtod
		foreach my $node (@m_nodes){
			my $linenumber = $node->getAttribute("line");
			my $m_name = $node->getAttribute("name");
			my $args = $node->getAttribute("desc");
		        $args =~ /\((.*)\)/;
			my $args_list = $1;

			if (not grep(/^$linenumber$/, @covered_lines)){ 
				@mth_infos=(@mth_infos,[$linenumber,"$m_name<$args_list>"]);
				@covered_lines = (@covered_lines, $linenumber);
			}
		}

                foreach my $subclass (keys %subclass_m_nodes){
			foreach my $sub_node (@{$subclass_m_nodes{$subclass}}){
                        	my $linenumber = $sub_node->getAttribute("line");
                        	my $m_name = $sub_node->getAttribute("name");
                        	my $args = $sub_node->getAttribute("desc");
                        	$args =~ /\((.*)\)/;
				my $args_list = $1;

				if (not grep(/^$linenumber$/, @covered_lines)){ 
                        		@mth_infos=(@mth_infos,[$linenumber,"$subclass".'$'."$m_name<$args_list>"]);
					@covered_lines = (@covered_lines, $linenumber);
				}
			}
                }	
		#sort method info arrays by first element 
		my @sorted_mth_infos = sort { $a->[0] <=> $b->[0]} @mth_infos; 
		$first_mth_line_num = $sorted_mth_infos[0][0];
		#create statement infos, nr: line number
		foreach my $node (@st_nodes){
			my $linenumber=$node->getAttribute("nr");
			my $stmt_info;

			if ($linenumber < $first_mth_line_num){
				$stmt_info = "$linenumber,class<>";
				#print $stmt_info."\n";
				@stmt_infos = (@stmt_infos, $stmt_info);
				next;
			}
			else{
				#if not in rang of current method
				if ($index < $#sorted_mth_infos){
					if ($linenumber >= $sorted_mth_infos[$index + 1][0]){
						$index++;
					}
				}
                                $stmt_info = "$linenumber,$sorted_mth_infos[$index][1]";
				#print $stmt_info."\n";
                                @stmt_infos=(@stmt_infos, $stmt_info);
			}
		}
			
		$line_infos_hash{$class}=\@stmt_infos;
		$index = 0;
	}

	return %line_infos_hash;
}

#return the subclass node attribute name for the class_name
#find subclass of class_name class. --> will be retrieved as cand_classes
sub find_subclasses{
	my ($class_name, @cand_classes) = @_;
	my @subclasses = ();
	
	foreach my $cand_class (@cand_classes){
		my $name = $cand_class->getAttribute("name");
		if ($name =~ /$class_name\$/){
			@subclasses = (@subclasses, $name);
		}
	}
	return @subclasses;
}

1;
