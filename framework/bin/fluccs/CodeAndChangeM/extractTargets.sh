#!/bin/bash
#$1 = working directory, $2 = src directory $3 = project id, $4 = bug number

mvn exec:java -Dexec.mainClass="FLUCCS.codeAndchange.ExtractMethods" -Dexec.args="$1 $2 $3 $4 $D4J_HOME"
