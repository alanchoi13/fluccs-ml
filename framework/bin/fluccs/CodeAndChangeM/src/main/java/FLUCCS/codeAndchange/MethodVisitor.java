package FLUCCS.codeAndchange;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.ast.body.Parameter;

import java.util.ArrayList;

import java.util.regex.*;

public class MethodVisitor extends VoidVisitorAdapter{
	private MethodProp declaredMethod = null;
	private String declaredClass;
	private String methodDesc;
	private int count = -1;

	public MethodVisitor(String declaredClass){
		this.declaredClass = declaredClass;
	}
	
	public void visit(MethodDeclaration m, int count){
		this.methodDesc = m.getName().toString();
		Pattern pt = Pattern.compile("<T extends ([^>]*>?)>\\s+.* " + this.methodDesc);
		Matcher matched = pt.matcher(m.removeComment().toString());
		String ExtendType = null;	
		if (matched.find()){
			ExtendType = matched.group(1);
		}

		String params = "<";
		for (Parameter p: m.getParameters()){
			params += GetParameterOrArgumentType.getType(p, ExtendType) + ";";
			
		}
		if (params.length() > 1){
			params = params.substring(0, params.length() - 1);
		}
		params += ">";
		this.methodDesc += "," + params;
		ArrayList<Integer> line_nums = GetRange.getLineNumbers(m);
		this.declaredMethod = new MethodProp(this.declaredClass, this.methodDesc, line_nums);

	}

	public MethodProp getDeclaredMethod(){
		return this.declaredMethod;
	}

	public int getUpdatedCount(){
		return this.count;
	}
}
