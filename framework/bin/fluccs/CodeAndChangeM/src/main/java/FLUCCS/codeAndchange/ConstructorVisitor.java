package FLUCCS.codeAndchange;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.regex.*;

import java.util.ArrayList;

public class ConstructorVisitor extends VoidVisitorAdapter{
	private MethodProp declaredMethod;
	private String declaredClass;
	private String constructorDesc;
	private int count = -1;

	public ConstructorVisitor(String declaredClass){
		this.declaredClass = declaredClass;
	}	

	public void visit(ConstructorDeclaration consDecl, int count){
		this.constructorDesc = "<init>";
		Pattern pt = Pattern.compile("<T extends ([^>]*>?)>\\s+.* " + this.declaredClass);
		Matcher matched = pt.matcher(consDecl.removeComment().toString());
		String ExtendType = null;
		if (matched.find()){
			ExtendType = matched.group(1);
		}

		String params = "<";
		for (Parameter p: consDecl.getParameters()){
			params += GetParameterOrArgumentType.getType(p, ExtendType) + ";";
		}
		if (params.length() > 1){
			params = params.substring(0, params.length() - 1);
		}
		params += ">";
		this.constructorDesc += "," + params;
		ArrayList<Integer> line_nums = GetRange.getLineNumbers(consDecl);
		this.declaredMethod = new MethodProp(this.declaredClass, this.constructorDesc, line_nums);
	}	

	public MethodProp getDeclaredMethod(){
		return this.declaredMethod;
	}

	public int getUpdatedCount(){
		return this.count;	
	}
}
