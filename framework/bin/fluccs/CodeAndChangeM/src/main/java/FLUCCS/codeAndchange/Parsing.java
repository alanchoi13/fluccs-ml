package FLUCCS.codeAndchange;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.IOException;

import java.nio.file.Paths;
import java.nio.file.Path;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;

import com.github.javaparser.ParseProblemException;
import com.github.javaparser.ast.validator.Java1_3Validator;
import com.github.javaparser.ParserConfiguration;
import static com.github.javaparser.ParseStart.*;
import static com.github.javaparser.Providers.provider;

import java.util.ArrayList;
import java.util.Optional;

public class Parsing{
	public static void writeDeclaredMethods(ArrayList<MethodProp> declaredMethods, String fileName){
		try{
			FileWriter fw = new FileWriter(fileName, false);
			BufferedWriter bw = new BufferedWriter(fw);
			for (MethodProp methodProp: declaredMethods){
				String line = methodProp.getDeclaredClass() + "," + methodProp.getMethodId() + "=";
				for (int linenum: methodProp.getLines()){
					line += Integer.toString(linenum) + ",";
				}
				line = line.substring(0, line.length() - 1);
				bw.write(line);
				bw.newLine();
				bw.flush();
			}
		} catch(IOException e){
			System.out.println("IOException occurred");
			System.out.println(e);
		}
	}


	public ArrayList<MethodProp> getDeclaredMethods(String fileName){
		ArrayList<MethodProp> declaredMethods = null;
		Path filepath = null;
		try{
			//System.out.println("Is " + fileName + " exists?:\t"); // logging
			//System.out.println(new File(fileName).isFile()); // logging
			filepath = Paths.get(fileName);
			CompilationUnit cu = null;
			try{
				cu = JavaParser.parse(filepath);
			} catch (ParseProblemException e){
				System.out.println("ParseProblemException occurred: enable java1.3 validator");
				JavaParser javaParser = new JavaParser(new ParserConfiguration().setValidator(new Java1_3Validator()));
				/*if (filepath == null){ // logging
					System.out.println("file path is null: " + filepath);
				}
				else{
					System.out.println("file path is not null: " + filepath);
					System.out.println(new File(fileName).isFile());
				}*/
				File targetParseFile = new File(fileName);
				Optional<CompilationUnit> Opcu = javaParser.parse(COMPILATION_UNIT, provider(targetParseFile)).getResult();
				cu = Opcu.get();
				System.out.println("Compilation unit is succesfully retrieved");
			}
			PackageVisitor p_visitor = new PackageVisitor();
			p_visitor.visit(cu, null);
			String packageName = p_visitor.getPackageName();
			NodeIterator nodeIterator = new NodeIterator();
			nodeIterator.explore(cu, packageName, 1);
			declaredMethods = nodeIterator.getDeclaredMethods();
		} catch (IOException e){
			System.out.println("IOException occurred while reading " + fileName);
			System.out.println(e);
			System.exit(0);
		}
		return declaredMethods;
	}

	public static void main(String[] args){
		String dest = args[1];
		if (dest.charAt(dest.length()-1) == '/'){
			dest = dest.substring(0, dest.length() - 1);
		}
		ArrayList<MethodProp> declaredMethods = new ArrayList<MethodProp>();
		declaredMethods.addAll(new Parsing().getDeclaredMethods(args[0]));
		writeDeclaredMethods(declaredMethods, dest + "/method_Linenums.csv");
	}
}
