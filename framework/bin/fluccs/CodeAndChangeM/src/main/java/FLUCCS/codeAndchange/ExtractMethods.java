package FLUCCS.codeAndchange;

import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;

import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;

import java.nio.file.Paths;

public class ExtractMethods {
	public static HashMap<String, String> readMthStmtInfo(String projectName, String bugNum, String D4J_HOME){
		HashMap<String, String> mthStmtInfo = new HashMap<String, String>();
		FileReader fr = null;
		BufferedReader br = null;
		String mth_stmtDir = Paths.get(D4J_HOME, "framework/bin/fluccs/method_stmt/", projectName).toString();
		String fileName = mth_stmtDir + "/stmt_mth_id_" + projectName + "_" + bugNum + ".csv";
		try{
			fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			String line = br.readLine();
			while (line != null){
				String[] temp = line.split(",");
				mthStmtInfo.put(temp[0] + ":" + temp[1], temp[2]);
				line = br.readLine();
			}
			if (br != null){
				br.close();	
			}
		} catch(IOException e){
			System.out.println(e);
			System.out.println("Error occurred while finding identifier");
		}
		return mthStmtInfo;
	}
	
	public static String findID(MethodProp methodProp, String fileName, HashMap<String, String> mthStmtInfo){
		String ID = null;
		//discard subclass part
		String declaredFileID = fileName.replace("/", ".");
		//discard '.java' part
		declaredFileID = declaredFileID.substring(0, declaredFileID.length() - 5);
		
		String methodId = methodProp.getMethodId();
		String methodName = methodId.substring(0, methodId.indexOf(','));
		ArrayList<Integer> lines = methodProp.getLines();

		Set<String> mthStmtInfoKeySet = mthStmtInfo.keySet();
		for (int lineNum: lines){
			String key = declaredFileID + ":" + Integer.toString(lineNum);
			if (mthStmtInfoKeySet.contains(key)){
				//check this is the real method or just a overlapped one
				String candID = mthStmtInfo.get(key);
				if (candID.contains(methodName + "<")){
					ID = candID;
					break;
				}
			}
		}

		try{
			if (ID == null){
				//System.out.println("Cannot find the matching ID: " + declaredFileID + "," +  methodProp.getDeclaredClass() + "," + methodId); --> where the current line is considered as non-executable by Cobertura
				throw new RuntimeException();
			}
		} catch (RuntimeException e){
			//System.out.println(e);
		} finally{
			return ID;
		}
	}


	public static ArrayList<String> writeFileLst(String srcDirName, String outfileName, String prefix, ArrayList<String> targetFiles){
		File srcDir = new File(srcDirName);
		File[] listOfFiles = srcDir.listFiles();
		BufferedWriter bw = null;
		if (srcDirName.endsWith("/")){
			srcDirName = srcDirName.substring(0, srcDirName.length() - 1);
		}

		//System.out.println("Writing the file: " + outfileName); // logging
		try{
			FileWriter fw = new FileWriter(outfileName, true);
			bw = new BufferedWriter(fw);
			for (File afile: listOfFiles){
				if (afile.isDirectory()){
					String nextPrefix = prefix;
					if (nextPrefix.length() > 0){
						nextPrefix += "/";
					}
					nextPrefix += afile.getName();
					writeFileLst(srcDirName + "/" + afile.getName(), outfileName, nextPrefix, targetFiles);
				} else if (afile.isFile()){
					String fileName = afile.getName();
					if (fileName.endsWith(".java")){
						if (prefix.length() > 0){
							fileName = prefix + "/" + fileName;
						}
						targetFiles.add(fileName);
						bw.write(fileName);
						bw.newLine();
						bw.flush();
					}
				}
			}
			if (bw != null){
				bw.close();
			}
		} catch (IOException e){
			System.out.println(e);
			System.out.println("IOException occurred whiel writing class.list");
			
		}
		return targetFiles;
	}


	public static void writeTargetMethods(Parsing parser, String srcDirName, ArrayList<String> file_lst, String projectName, String bugNum, String D4J_HOME){
		HashMap<String, String> mthStmtInfo = readMthStmtInfo(projectName, bugNum, D4J_HOME);
		Set<String> foundMethods = new HashSet<String>();
		
		FileWriter fw = null;
		//FileWriter fwNull = null; // for logging
		BufferedWriter bw = null;
		//BufferedWriter bwNull = null; // for logging 
		String mth_stmtDir = Paths.get(D4J_HOME, "framework/bin/fluccs/method_stmt/", projectName).toString();

		try{
			fw = new FileWriter(mth_stmtDir + "/Methods/" + projectName + "." + bugNum + ".csv");
			bw = new BufferedWriter(fw);
			//fwNull = new FileWriter(mth_stmtDir + "/Methods/Null." + projectName + "." + bugNum + ".csv");
			//bwNull = new BufferedWriter(fwNull); // for recording a list of methods which are considered as non-executalbe when using COBERTURA
			//for each file, get declared methods
			for (String fileName: file_lst){
				if (srcDirName.endsWith("/")){
					srcDirName = srcDirName.substring(0, srcDirName.length() - 1);
				}
				String filePath = srcDirName + "/" + fileName;
				//get declard methods from the current file
				ArrayList<MethodProp> methodProps = parser.getDeclaredMethods(filePath);
				//for each declared method, find the complete written method identifier
				for (MethodProp methodProp: methodProps){
					String ID = findID(methodProp, fileName, mthStmtInfo);
					String methodDesc = methodProp.getDeclaredClass() + "," + methodProp.getMethodId();
					if (ID != null){
						foundMethods.add(ID);
						bw.write(ID + "," + fileName + "," + methodDesc);
						bw.newLine();
						bw.flush();	
					}
					/*else{// logging
						System.out.println("there is no mathcing for " + methodProp.getMethodId());
						if (foundMethods.contains(ID)){
							System.out.println("SAME OCCURRED: " + ID);
						}
						bwNull.write(ID + "," + fileName + "," + methodDesc);
						bwNull.newLine();
						bwNull.flush();
					}*/
				}
				if( methodProps.size() == 0){
					System.out.println("===" + fileName);
				}
				
			}
			if (bw != null){
				bw.close();
			}
			/*if (bwNull != null){
				bwNull.close();
			}*/
		} catch (IOException e){
			System.out.println(e);
			System.out.println("In writeTargetMethods, error ocucred during writing");
		}

	}

	public static void main(String[] args){
		String workDirName = args[0];
		String srcDirName = args[1];
		String projectName = args[2];
		String bugNum = args[3];
		String D4J_HOME = args[4];

		Parsing parser = new Parsing();
		if (workDirName.endsWith("/")){
			workDirName = workDirName.substring(0, workDirName.length() - 1);
		}
		String javaFileLstName = workDirName + "/targetFile.csv";
		File javaFileLstPath = new File(javaFileLstName);
		FileReader fr = null;
		BufferedReader br = null;
		ArrayList<String> targetFiles = new ArrayList<String>();
		try{
			if (javaFileLstPath.exists()){
				fr = new FileReader(javaFileLstPath);
				br = new BufferedReader(fr);
				String line = br.readLine();
				while(line != null){
					String filePath = line.replace("\n", "");
					targetFiles.add(filePath);
					line = br.readLine();
				}
			}
			else{
				targetFiles = writeFileLst(srcDirName, javaFileLstName, "", targetFiles);
			}
			if (br != null){
				br.close();
			}
		} catch (IOException e){
			System.out.println(e);
			System.out.println("Error occurred while reading class list file");
		}

		writeTargetMethods(parser, srcDirName, targetFiles, projectName, bugNum, D4J_HOME);
	}

}
