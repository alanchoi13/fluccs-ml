package FLUCCS.codeAndchange;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.ObjectCreationExpr;

import java.util.regex.*;
import java.util.ArrayList;

public class NodeIterator{
	private ArrayList<MethodProp> declaredMethods; 
	private ArrayList<String> declaredClassName;
	private String inFile;

	public NodeIterator(){
		this.declaredMethods = new ArrayList<MethodProp>();
		this.declaredClassName = new ArrayList<String>();
	}

	public static boolean checkContinue(Node node){
		boolean boolContinue = false;
		for (Node child: node.getChildNodes()){
			if (node instanceof MethodDeclaration){
				boolContinue = true;
				break;
			}
			else{
				boolContinue = checkContinue(child);
			}
		}
		return boolContinue;
	}

	public int handle(Node node, String className, int count){
		//MethodProp sub_declaredMethod;
		ArrayList<MethodProp> sub_declaredMethods = new ArrayList<MethodProp>();
	 	if (node instanceof ConstructorDeclaration){
			//System.out.println("in ConstructorDeclaration"); // logging
			ConstructorVisitor consv = new ConstructorVisitor(className);
			consv.visit((ConstructorDeclaration)node, -1);
			sub_declaredMethods.add(consv.getDeclaredMethod());
		}
		else if (node instanceof MethodDeclaration){
			//System.out.println("in MethodDeclaration"); // logging
			MethodVisitor mv = new MethodVisitor(className);
			mv.visit((MethodDeclaration)node, -1);
			sub_declaredMethods.add(mv.getDeclaredMethod());
		}
		else if (node instanceof ObjectCreationExpr){
			//System.out.println("in ObjectCreationExpr: " + Integer.toString(count)); // logging
			ObjectCreationVisitor ocv = new ObjectCreationVisitor(className, count);
			ocv.visit((ObjectCreationExpr)node, -1);
			MethodProp tempMethodProp = ocv.getDeclaredMethod();
			if (tempMethodProp != null){
				sub_declaredMethods.add(tempMethodProp);
				count ++;
			}
			else{
				return count;
			}
		}
		else{
			return count;
		}

		if (!this.declaredClassName.contains(className)){
			this.declaredClassName.add(className);
		}
		this.declaredMethods.addAll(sub_declaredMethods);
		return count;
	}
	
	public int checkContain(String aclassName, String subClassName){
		String findedString = null;
		int count = 0;
		String className = aclassName.replace("$", "\\$");
		String strPattern = "^" + className + "\\$([0-9]+)" + subClassName + "$";
		Pattern pattern = Pattern.compile(strPattern);
		for (String aDeclaredClassName: this.declaredClassName){
			Matcher match = pattern.matcher(aDeclaredClassName);
			if (match.find()){
				int temp_count = Integer.parseInt(match.group(1));
				if (temp_count > count){
					count = temp_count;
				}
			}
		}
		return count;
	}

	public int explore(Node node, String className, int count){
		count = handle(node, className, count);
		for (Node child: node.getChildNodes()){
			String currentClassName = className;
			if (child instanceof ClassOrInterfaceDeclaration){
				if (((ClassOrInterfaceDeclaration)child).isLocalClassDeclaration()){
					String subClassName = ((ClassOrInterfaceDeclaration)child).getName().toString();
					int _count = checkContain(currentClassName, subClassName); 
					
					if (_count > 0){
						_count ++;
					}
					else{
						_count = 1;
					}
					currentClassName += "$" + Integer.toString(_count) + subClassName;
				}
				else{
					currentClassName += "." + ((ClassOrInterfaceDeclaration)child).getName();
				}
			}
			else{
				if (child instanceof ObjectCreationExpr && checkContinue(child)){
					currentClassName += "$" + Integer.toString(count);
				}
			}
			count = explore(child, currentClassName, count);
		}
		return count;
	}


	public ArrayList<MethodProp> getDeclaredMethods(){
		
		for (MethodProp methodProp: this.declaredMethods){
			for (MethodProp compMethodProp: this.declaredMethods){
				//delete the overlapped part
				if (!methodProp.getMethodId().equals(compMethodProp.getMethodId()) && 
					methodProp.getDeclaredClass().equals(compMethodProp.getDeclaredClass())){
					if (methodProp.getLines().get(0) >= compMethodProp.getLines().get(0) || methodProp.getLines().equals(compMethodProp.getLines())){
						continue;
					}
					else{
						//if none are common, none are deleted
						methodProp.deleteLines(compMethodProp.getLines());
					}
				}
			}
		}
	
		return this.declaredMethods;
	}
	

}
