package FLUCCS.codeAndchange;

import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class PackageVisitor extends VoidVisitorAdapter{
	private String packageName = null;
	
	public void visit(PackageDeclaration pdc, Object args){
		this.packageName = pdc.getName().toString();
	}

	public String getPackageName(){
		return this.packageName;
	}

}
