package FLUCCS.codeAndchange;

import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.expr.Expression;

import java.util.regex.*;
import java.util.ArrayList;


public class ObjectCreationVisitor extends VoidVisitorAdapter{
	private String declaredClass;	
	private int count;
	private boolean in = false;
	private MethodProp declaredMethod = null;

	public ObjectCreationVisitor(String declaredClass, int count){
		this.declaredClass = declaredClass;
		this.count = count;
	}

	public void visit(ObjectCreationExpr oc, Object arg){
		boolean in = NodeIterator.checkContinue(oc);
		if (in){
			String ocID = this.declaredClass;
			Pattern pt = Pattern.compile("<T extends ([^>]*>?)>\\s+.* " + ocID);
			Matcher matched = pt.matcher(oc.removeComment().toString());
			String ExtendType = null; 
			if (matched.find()){
				ExtendType = matched.group(1);
			}

			String initKey = "<init>";
			String args = "<";
			for (Expression p: oc.getArguments()){
				String tempArg = GetParameterOrArgumentType.getType(p, ExtendType);
				if (tempArg != null){
					args += tempArg + ";";
				}
			}
			if (args.length() > 1){
				args.substring(0, args.length() - 1);
			}
			args += ">";
			initKey += "," + args;
			this.declaredMethod = new MethodProp(this.declaredClass, initKey, GetRange.getLineNumbers(oc));

		}
	}
	
	public MethodProp getDeclaredMethod(){
		return declaredMethod;
	}

	public int getUpdatedCount(){
		return this.count;	
	}

} 
