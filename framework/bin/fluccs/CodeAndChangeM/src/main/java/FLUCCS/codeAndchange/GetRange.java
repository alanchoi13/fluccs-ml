package FLUCCS.codeAndchange;

import com.github.javaparser.ast.Node;
import java.util.ArrayList;

public class GetRange{
	public static ArrayList<Integer> getLineNumbers(Node node){
		int begin_num = node.getBegin().get().line;
		int end_num = node.getEnd().get().line;
		ArrayList<Integer> lineNumbers = new ArrayList<Integer>();	
		for (int idx = begin_num; idx <= end_num; idx++){
			lineNumbers.add(idx);
		}
		return lineNumbers;	
	}
}
