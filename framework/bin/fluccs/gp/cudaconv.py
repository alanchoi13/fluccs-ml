#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
import pickle, sys, os
from deap import gp, tools, creator, base
import numpy

import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule

#Generate argument dictionary(global arg_dict): key: argument(feature)_number, value: feature
def make_arg_dict():
	arg_dict = dict()

	arg_dict["ARG0"] = "ochiai[tid]"
	arg_dict["ARG1"] = "jaccard[tid]"
	arg_dict["ARG2"] = "gp13[tid]"
	arg_dict["ARG3"] = "wong1[tid]"
	arg_dict["ARG4"] = "wong2[tid]"
	arg_dict["ARG5"] = "wong3[tid]"
	arg_dict["ARG6"] = "tarantula[tid]"
	arg_dict["ARG7"] = "ample[tid]"
	arg_dict["ARG8"] = "RussellRao[tid]"
	arg_dict["ARG9"] = "SorensenDice[tid]"
	arg_dict["ARG10"] = "Kulczynski1[tid]"
	arg_dict["ARG11"] = "SimpleMatching[tid]"
	arg_dict["ARG12"] = "M1[tid]"
	arg_dict["ARG13"] = "RogersTanimoto[tid]"
	arg_dict["ARG14"] = "Hamming[tid]"
	arg_dict["ARG15"] = "Ochiai2[tid]"
	arg_dict["ARG16"] = "Hamann[tid]"
	arg_dict["ARG17"] = "dice[tid]"
	arg_dict["ARG18"] = "Kulczynski2[tid]"
	arg_dict["ARG19"] = "Sokal[tid]"
	arg_dict["ARG20"] = "M2[tid]"
	arg_dict["ARG21"] = "Goodman[tid]"
	arg_dict["ARG22"] = "Euclid[tid]"
	arg_dict["ARG23"] = "Anderberg[tid]"
	arg_dict["ARG24"] = "Zoltar[tid]"
	arg_dict["ARG25"] = "ER1a[tid]"
	arg_dict["ARG26"] = "ER1b[tid]"
	arg_dict["ARG27"] = "ER5a[tid]"
	arg_dict["ARG28"] = "ER5b[tid]"
	arg_dict["ARG29"] = "ER5c[tid]"
	arg_dict["ARG30"] = "gp02[tid]"
	arg_dict["ARG31"] = "gp03[tid]"
	arg_dict["ARG32"] = "gp19[tid]"
	arg_dict["ARG33"] = "churn[tid]"
	arg_dict["ARG34"] = "max_age[tid]"
	arg_dict["ARG35"] = "min_age[tid]"
	arg_dict["ARG36"] = "num_args[tid]"
	arg_dict["ARG37"] = "num_vars[tid]"
	arg_dict["ARG38"] = "b_length[tid]"
	arg_dict["ARG39"] = "loc[tid]"

	return arg_dict	


#Convert the structure of individual, which is a tree, into String
def getCLExpression(individual, arg_dict, begin = 0):

	str_buf = ""
	sub_form = individual.searchSubtree(begin)
	start = sub_form.start
	stop = sub_form.stop
	length = stop - start
	
	is_leaf = True if (length == 1) else False
	#print str(individual[begin].arity) + "," + str(individual[begin].name)
	
	if not is_leaf:
		op = str(individual[begin].name)
		begin += 1
		if (op != "gp_sqrt" and op != "gp_neg"):
			(l_child_buf, begin) = getCLExpression(individual, arg_dict, begin)
			(r_child_buf, begin) = getCLExpression(individual, arg_dict, begin)
			if (op == "gp_div"):
				str_buf = op + "(" + l_child_buf + "," + r_child_buf + ")"
			else:
				if (op == "gp_add"):
					op = "+"
				elif (op == "gp_sub"):
					op = "-"
				elif (op == "gp_mul"):
					op = "*"
				str_buf = "(" + l_child_buf + " " + op + " " + r_child_buf + ")"
		else:
			(child_buf, begin) = getCLExpression(individual, arg_dict, begin)
			if (op == "gp_neg"):
				op = "-"
			str_buf = op + "(" + child_buf + ")"
			
	else:#is leaf
		terminal = str(individual[begin].name)
		if (terminal != "1.0"):
			terminal = arg_dict[terminal]
		begin += 1
		str_buf = terminal
	
	return (str_buf, begin)


#CUDATool
#initiate, load, compile, compute using GPGPU
class CUDATool:
	def __init__(self):
		self.local_workgroup_size = 512
		self.gpu_list = []
		self.grid_length = []
		self.padded_length = []
		self.length = []
		self.result_list = []
		self.mod = None

		
	def empty_list(self):
		self.gpu_list = []; self.grid_length = []; self.padded_length = []; self.length = []; self.result_list = []

	#load cuda kernel file	
	def loadkernels(self, individuals=None):
		cudaFileDir = os.path.dirname(os.path.abspath(__file__))
		template = "".join(open(os.path.join(cudaFileDir, "kernel_template.cuda"), "r").readlines())
		if individuals:
			for i in range(len(individuals)):
				cltemp = str(individuals[i])
				if (i < 9):
					template = template.replace("12340" + str(i+1), cltemp)
				else:
					template = template.replace("1234" + str(i+1), cltemp)
		return template

	#Copy entire data to gpu before starting the computation(evaluation of individual with current data)
	def mem_cp(self, datas):
		self.empty_list()
		count = 0
		for data in datas:
			self.gpu_list.append(list())
			(fault_index, vectors) = data
			length = 0
			length = len(vectors)
			padded_length = (length / self.local_workgroup_size + 1) * self.local_workgroup_size
			grid_x = padded_length / self.local_workgroup_size
			
			ochiais = numpy.zeros(padded_length, dtype=numpy.float32)
			ochiais[:length] = [numpy.float32(v[1]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(ochiais.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], ochiais)	

			jaccards = numpy.zeros(padded_length, dtype=numpy.float32)
			jaccards[:length] = [numpy.float32(v[2]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(jaccards.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], jaccards)

			gp13s = numpy.zeros(padded_length, dtype=numpy.float32)
			gp13s[:length] = [numpy.float32(v[3]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(gp13s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], gp13s)

			wong1s = numpy.zeros(padded_length, dtype=numpy.float32)
			wong1s[:length] = [numpy.float32(v[4]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(wong1s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], wong1s)

			wong2s = numpy.zeros(padded_length, dtype=numpy.float32)
			wong2s[:length] = [numpy.float32(v[5]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(wong2s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], wong2s)

			wong3s = numpy.zeros(padded_length, dtype=numpy.float32)
			wong3s[:length] = [numpy.float32(v[6]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(wong3s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], wong3s)
		
			tarantulas = numpy.zeros(padded_length, dtype=numpy.float32)
			tarantulas[:length] = [numpy.float32(v[7]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(tarantulas.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], tarantulas)

			amples = numpy.zeros(padded_length, dtype=numpy.float32)
			amples[:length] = [numpy.float32(v[8]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(amples.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], amples)

			RussellRaos = numpy.zeros(padded_length, dtype=numpy.float32)
			RussellRaos[:length] = [numpy.float32(v[9]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(RussellRaos.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], RussellRaos)

			SorensenDices = numpy.zeros(padded_length, dtype=numpy.float32)
			SorensenDices[:length] = [numpy.float32(v[10]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(SorensenDices.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], SorensenDices)

			Kulczynski1s = numpy.zeros(padded_length, dtype=numpy.float32)
			Kulczynski1s[:length] = [numpy.float32(v[11]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Kulczynski1s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Kulczynski1s)

			SimpleMatchings = numpy.zeros(padded_length, dtype=numpy.float32)
			SimpleMatchings[:length] = [numpy.float32(v[12]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(SimpleMatchings.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], SimpleMatchings)

			M1s = numpy.zeros(padded_length, dtype=numpy.float32)
			M1s[:length] = [numpy.float32(v[13]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(M1s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], M1s)

			RogersTanimotos = numpy.zeros(padded_length, dtype=numpy.float32)
			RogersTanimotos[:length] = [numpy.float32(v[14]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(RogersTanimotos.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], RogersTanimotos)

			Hammings = numpy.zeros(padded_length, dtype=numpy.float32)
			Hammings[:length] = [numpy.float32(v[15]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Hammings.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Hammings)

			Ochiai2s = numpy.zeros(padded_length, dtype=numpy.float32)
			Ochiai2s[:length] = [numpy.float32(v[16]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Ochiai2s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Ochiai2s)

			Hamanns = numpy.zeros(padded_length, dtype=numpy.float32)
			Hamanns[:length] = [numpy.float32(v[17]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Hamanns.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Hamanns)

			Dices = numpy.zeros(padded_length, dtype=numpy.float32)
			Dices[:length] = [numpy.float32(v[18]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Dices.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Dices)

			Kulczynski2s = numpy.zeros(padded_length, dtype=numpy.float32)
			Kulczynski2s[:length] = [numpy.float32(v[19]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Kulczynski2s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Kulczynski2s)

			Sokals = numpy.zeros(padded_length, dtype=numpy.float32)
			Sokals[:length] = [numpy.float32(v[20]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Sokals.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Sokals)

			M2s = numpy.zeros(padded_length, dtype=numpy.float32)
			M2s[:length] = [numpy.float32(v[21]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(M2s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], M2s)

			Goodmans = numpy.zeros(padded_length, dtype=numpy.float32)
			Goodmans[:length] = [numpy.float32(v[22]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Goodmans.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Goodmans)

			Euclids = numpy.zeros(padded_length, dtype=numpy.float32)
			Euclids[:length] = [numpy.float32(v[23]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Euclids.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Euclids)

			Anderbergs = numpy.zeros(padded_length, dtype=numpy.float32)
			Anderbergs[:length] = [numpy.float32(v[24]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Anderbergs.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Anderbergs)

			Zoltars = numpy.zeros(padded_length, dtype=numpy.float32)
			Zoltars[:length] = [numpy.float32(v[25]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(Zoltars.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], Zoltars)

			ER1as = numpy.zeros(padded_length, dtype=numpy.float32)
			ER1as[:length] = [numpy.float32(v[26]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(ER1as.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], ER1as)

			ER1bs = numpy.zeros(padded_length, dtype=numpy.float32)
			ER1bs[:length] = [numpy.float32(v[27]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(ER1bs.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], ER1bs)

			ER5as = numpy.zeros(padded_length, dtype=numpy.float32)
			ER5as[:length] = [numpy.float32(v[28]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(ER5as.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], ER5as)
	
			ER5bs = numpy.zeros(padded_length, dtype=numpy.float32)
			ER5bs[:length] = [numpy.float32(v[29]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(ER5bs.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], ER5bs)

			ER5cs = numpy.zeros(padded_length, dtype=numpy.float32)
			ER5cs[:length] = [numpy.float32(v[30]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(ER5cs.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], ER5cs)

			gp02s = numpy.zeros(padded_length, dtype=numpy.float32)
			gp02s[:length] = [numpy.float32(v[31]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(gp02s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], gp02s)

			gp03s = numpy.zeros(padded_length, dtype=numpy.float32)
			gp03s[:length] = [numpy.float32(v[32]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(gp03s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], gp03s)

			gp19s = numpy.zeros(padded_length, dtype=numpy.float32)
			gp19s[:length] = [numpy.float32(v[33]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(gp19s.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], gp19s)

			churns = numpy.zeros(padded_length, dtype=numpy.float32)
			churns[:length] = [numpy.float32(v[34]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(churns.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], churns)

			max_ages = numpy.zeros(padded_length, dtype=numpy.float32)
			max_ages[:length] = [numpy.float32(v[35]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(max_ages.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], max_ages)

			min_ages = numpy.zeros(padded_length, dtype=numpy.float32)
			min_ages[:length] = [numpy.float32(v[36]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(min_ages.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], min_ages)

			num_args = numpy.zeros(padded_length, dtype=numpy.float32)
			num_args[:length] = [numpy.float32(v[37]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(num_args.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], num_args)

			num_vars = numpy.zeros(padded_length, dtype=numpy.float32)
			num_vars[:length] = [numpy.float32(v[38]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(num_vars.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], num_vars)

			b_lengths = numpy.zeros(padded_length, dtype=numpy.float32)
			b_lengths[:length] = [numpy.float32(v[39]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(b_lengths.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], b_lengths)

			locs = numpy.zeros(padded_length, dtype=numpy.float32)
			locs[:length] = [numpy.float32(v[40]) for v in vectors]
			self.gpu_list[count].append(drv.mem_alloc(locs.nbytes))
			drv.memcpy_htod(self.gpu_list[count][-1], locs)

			sps = numpy.array([0.0] * padded_length, dtype=numpy.float32)
			self.result_list.append(drv.mem_alloc(sps.nbytes))
			drv.memcpy_htod(self.result_list[-1], sps)
			
			self.grid_length.append(grid_x)
			self.padded_length.append(padded_length)
			self.length.append(length)
			count += 1

	#Compile evaluation functions for entire individual set - each represented as evaluate_(the order of individual)
	#This is to reduce the compile time by compiling only once instead of compiling whenever evaluation is needed
	def Compile(self, individuals):
		kernel_code = self.loadkernels(individuals)
		self.mod = SourceModule(kernel_code)
	
	#Core function: compute score for current given data	
	def core(self, num, ind_index):
		sps = numpy.array([0.0] * self.padded_length[num], dtype=numpy.float32)

		individual_kernel = self.mod.get_function("evaluate_" + str(ind_index + 1))
	
		#access gpu data for num current data	
		individual_kernel(self.gpu_list[num][0], self.gpu_list[num][1], self.gpu_list[num][2], self.gpu_list[num][3], self.gpu_list[num][4], self.gpu_list[num][5], self.gpu_list[num][6], self.gpu_list[num][7], self.gpu_list[num][8], self.gpu_list[num][9], self.gpu_list[num][10], self.gpu_list[num][11], self.gpu_list[num][12], self.gpu_list[num][13], self.gpu_list[num][14], self.gpu_list[num][15], self.gpu_list[num][16], self.gpu_list[num][17], self.gpu_list[num][18], self.gpu_list[num][19], self.gpu_list[num][20], self.gpu_list[num][21], self.gpu_list[num][22], self.gpu_list[num][23], self.gpu_list[num][24], self.gpu_list[num][25], self.gpu_list[num][26], self.gpu_list[num][27], self.gpu_list[num][28], self.gpu_list[num][29], self.gpu_list[num][30], self.gpu_list[num][31], self.gpu_list[num][32], self.gpu_list[num][33], self.gpu_list[num][34], self.gpu_list[num][35], self.gpu_list[num][36], self.gpu_list[num][37], self.gpu_list[num][38], self.gpu_list[num][39], self.result_list[num], block=(512, 1, 1), grid=(self.grid_length[num], 1))
		
		drv.memcpy_dtoh(sps, self.result_list[num])
		sps = sps[:self.length[num]]
	
		return sps
