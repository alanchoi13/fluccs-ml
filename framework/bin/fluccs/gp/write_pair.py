#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
write 10 pairs of training data and test data.
"""

import os, sys
import csv, math
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import parser_check

def write_pair(datadir):
	csvWriter = csv.writer(open("pair.txt", "w"))
	datafiles = os.listdir(datadir)
	fold_size = math.ceil(len(datafiles)/float(10))
	print "Fold size for 10 cross fold validation is " + str(fold_size)
	index = 0; row = []; count = 0
	for i in range(len(datafiles)):
		if (i % fold_size == 0):
			csvWriter.writerow([str(index),])
			row = []
			index += 1

		if (count < fold_size - 1 and i != len(datafiles) - 1):
			row.append(datafiles[i])
			count += 1
		else:
			row.append(datafiles[i])
			count = 0
			csvWriter.writerow(list(set(datafiles) - set(row)))
			csvWriter.writerow(row)



if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None, help = "a directory where target data files are stored")
	args = parser.parse_args()
	parser_check.argument_check(parser, ["datadir"], [args.datadir])

	write_pair(args.datadir)	
