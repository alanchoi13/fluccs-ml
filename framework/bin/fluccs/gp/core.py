#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

import pickle, math, sys, os, random, numpy
from pyevolve import *
from deap import gp, tools, creator, base, algorithms
import operator
#import cudaconv
import ranking_w_seed
import cross_validation
import time
import csv
import re
import argparse
from scipy.stats import rankdata
#not sure ***
from gpOpeartors import *
# ***

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import parser_check

training_set = None
target_datas = []
toolbox = None
training_datas = []
current_datas = []

seed = None
arg_dict = dict()

cudatool = None
usegpu = None
#cudatool = cudaconv.CUDATool()
individuals = list()

#not sure ***
#def gp_add(a, b): return a+b
#def gp_sub(a, b): return a-b
#def gp_mul(a, b): return a*b
#def gp_div(a, b): return 1 if b == 0 else a/b
#def gp_sqrt(a): return math.sqrt(abs(a))
#def gp_neg(a): return -a
# ***

#a list of features used in the gp
features = ["ochiai", "jaccard", "gp13", "wong1", "wong2", "wong3", "tarantula", "ample", "RussellRao",\
	"SorensenDice", "Kulczynski1", "SimpleMatching", "M1", "RogersTanimoto", "Hamming", "Ochiai2",\
	"Hamann", "dice", "Kulczynski2", "Sokal", "M2", "Goodman", "Euclid", "Anderberg", "Zoltar",\
	"ER1a", "ER1b", "ER5a", "ER5b", "ER5c", "gp02", "gp03", "gp19", "churn",\
	"max_age","min_age", "num_args", "num_vars", "b_length", "loc"]

#Ranking Function: to generate rankings with random-tie-breaker with seed
def rank(re_vector):
	global seed
	return ranking_w_seed.rank(re_vector, ties="random", decreasing=True, seed = seed)

#return list of individual done with clexpression processing
#this method is executed only if usegpu == 1
def get_individual_list(population):
	global arg_dict
	individuals = list()

	for ind in population:
		(temp, _) = cudaconv.getCLExpression(ind, arg_dict)
		individuals.append(temp)

	return individuals

#evaluate individual without using gpu
def eval_wo_gpu(individual, current_data):
	global features
	(_, vectors) = current_data
	length = len(vectors)
#	print "in eval_wo_gpu"
#	print length
	sps = numpy.array([0.0] * length, dtype=numpy.float32)

	#get data
	ochiai = [numpy.float32(v[1]) for v in vectors]	
	jaccard = [numpy.float32(v[2]) for v in vectors]
	gp13 = [numpy.float32(v[3]) for v in vectors]
	wong1 = [numpy.float32(v[4]) for v in vectors]
	wong2 = [numpy.float32(v[5]) for v in vectors]
	wong3 = [numpy.float32(v[6]) for v in vectors]		
	tarantula = [numpy.float32(v[7]) for v in vectors]
	ample = [numpy.float32(v[8]) for v in vectors]
	RussellRao = [numpy.float32(v[9]) for v in vectors]	
	SorensenDice = [numpy.float32(v[10]) for v in vectors]
	Kulczynski1 = [numpy.float32(v[11]) for v in vectors]
	SimpleMatching = [numpy.float32(v[12]) for v in vectors]
	M1 = [numpy.float32(v[13]) for v in vectors]
	RogersTanimoto = [numpy.float32(v[14]) for v in vectors]
	Hamming = [numpy.float32(v[15]) for v in vectors]	
	Ochiai2 = [numpy.float32(v[16]) for v in vectors]	
	Hamann = [numpy.float32(v[17]) for v in vectors]
	dice = [numpy.float32(v[18]) for v in vectors]	
	Kulczynski2 = [numpy.float32(v[19]) for v in vectors]
	Sokal = [numpy.float32(v[20]) for v in vectors]
	M2 = [numpy.float32(v[21]) for v in vectors]
	Goodman = [numpy.float32(v[22]) for v in vectors]
	Euclid = [numpy.float32(v[23]) for v in vectors]
	Anderberg = [numpy.float32(v[24]) for v in vectors]
	Zoltar = [numpy.float32(v[25]) for v in vectors]
	ER1a = [numpy.float32(v[26]) for v in vectors]
	ER1b = [numpy.float32(v[27]) for v in vectors]
	ER5a = [numpy.float32(v[28]) for v in vectors]
	ER5b = [numpy.float32(v[29]) for v in vectors]
	ER5c = [numpy.float32(v[30]) for v in vectors]
	gp02 = [numpy.float32(v[31]) for v in vectors]
	gp03 = [numpy.float32(v[32]) for v in vectors]
	gp19 = [numpy.float32(v[33]) for v in vectors]
	churn = [numpy.float32(v[34]) for v in vectors]
	max_age = [numpy.float32(v[35]) for v in vectors]
	min_age = [numpy.float32(v[36]) for v in vectors]	
	num_args = [numpy.float32(v[37]) for v in vectors]
	num_vars = [numpy.float32(v[38]) for v in vectors]
	b_length = [numpy.float32(v[39]) for v in vectors]
	loc = [numpy.float32(v[40]) for v in vectors]

	for feature in features:
		individual = individual.replace(feature, feature + "[idx]")

#	print "replaced result: " + individual 

	#compute suspicousness score for each data row(per method) in current_data	
	for idx in range(length):
		sps[idx] = eval(individual)
	
	return sps

#Evaluate individual having ind_index(individual index) 
def eval_func(ind_index):
	global usegpu, cudatool, toolbox, current_datas, individuals
	fitness_values = []
	
#	print "in eval"
#	print len(individuals)
#	print individuals

	for i in range(len(current_datas)):
		if (bool(usegpu)):
			core_values = cudatool.core(i, ind_index)
		else:
#			print "====="
#			print ind_index
#			print individuals[ind_index]
#			print "===="
#			print i
#			print "+++++"
			core_values = eval_wo_gpu(individuals[ind_index], current_datas[i])
		#core_ranks = rank(core_values) #rank with random-tie-breaker
		core_ranks = rankdata(core_values, method = "max")
		core_rank = []
		(fault_index, _ ) = current_datas[i]
		
		for fault in fault_index:
			core_rank.append(normalise_rank(core_ranks, int(fault)))

		fitness = numpy.float32(min(core_rank))
		fitness_values.append(fitness)
	#Logging
	print "average ",individuals[ind_index], numpy.average(fitness_values)
	return (numpy.average(fitness_values),)


#Return percent value list for ranks
def normalise_rank(ranks, index):
	return numpy.float32(ranks[index]) / numpy.float32(len(ranks)) * 100

#Write final results of test data
def write_result(result, dest, best_fit, result_id):
	result_filename = result_id + ".result.csv"
	if (not os.path.exists(dest)):
		os.mkdir(dest)
	csvWriter = csv.writer(open(os.path.join(dest, result_filename), "w"))
	csvWriter.writerow([result, best_fit])
	

#Main function
#initialize training and testing data using pair.txt(10 cross validation: contain pair of training and test data), 
#set cudatool class for GPGPU usage 
#evolving and write the result of testing data into final_output(result directory)
#First, set current training and test(evaluation) data using pair.txt: Choose data fold to use
#Second, call evolve function to get 8 best candidate formula from current evolution
#Third, evaluate test(evaluation) data with each candidate and select the best
def experiment(data_dir, dest, result_id, pairFile, pair_id, maxTreeDepth, minTreeDepth, initMaxTreeDepth, seed = None): 
	global training_set, usegpu, cudatool, training_datas, arg_dict, individuals
	print "in experiment"

	if (bool(usegpu)):
		arg_dict = cudaconv.make_arg_dict()

	pair_datas = dict()
	#initialze training and test data: 10 cross validation 
	pair_datas = cross_validation.get_10_cross_validation(pairFile, data_dir)
	
	training_set = pair_datas[str(pair_id)][0]
	evaluation_set = pair_datas[str(pair_id)][1]

	for training_file in training_set:
		training_datas.append(pickle.load(open(training_file,"r")))

	evaluation_datas = []
	for evaluation_file in evaluation_set:
		evaluation_datas.append(pickle.load(open(evaluation_file, "r")))

	print "data settting finished"

	best = evolve(maxTreeDepth, minTreeDepth, initMaxTreeDepth, seed)
	#Logging
	print "candidates: "
	print best

	cand_values = dict()

	if (bool(usegpu)):	
		eval_cudatool = cudaconv.CUDATool()

		individuals = get_individual_list(best)
		eval_cudatool.Compile(individuals)
		eval_cudatool.mem_cp(evaluation_datas)
	else:
		individuals = [str(indv) for indv in best]

	#ind_index = 0
	for (ind_index, cand) in enumerate(best):
		fitness_values = []
		for i in range(len(evaluation_datas)):
			#where gpu is used
			if (bool(usegpu)):
				core_values = eval_cudatool.core(i, ind_index)
			else:
				core_values = eval_wo_gpu(individuals[ind_index], evaluation_datas[i])
			#core_ranks = rank(core_values)
			core_ranks = rankdata(core_values, method = "max")
			norm_core_ranks = []
			(fault_index, _) = evaluation_datas[i]
			for fault in fault_index:
				norm_core_ranks.append(normalise_rank(core_ranks, int(fault)))
			fitness = numpy.float32(min(norm_core_ranks))
			fitness_values.append(fitness)
		cand_values[str(cand)] = numpy.average(fitness_values)
		#ind_index += 1
	
	result_form = ""; best_fit = -1
	for key in cand_values.keys():
		if (best_fit == -1):
			best_fit = cand_values[key]; result_form = key
			continue
		else:
			if (best_fit > cand_values[key]):
				best_fit = cand_values[key]; result_form = key

	result = (result_form, list(training_set))
	print "\nThe best individual "
	print "\t\t expressin : " + str(result_form)
	print "\t\t fitness   : " + str(best_fit)
	print str(result)
	print "evaultaion set:"
	print list(evaluation_set)
	write_result(str(result_form), dest, str(best_fit), result_id)

#get initial population to apply gp and number of generations ngen 
#apply elitism --> previous best solutions can remain to next generation population 
#Return population after ngen generation of evolution
def gp_w_elit(population, ngen):
	global usegpu, toolbox, training_set, current_datas, training_datas, cudatool, individuals

	print "in gp_w_elit"
	stats = tools.Statistics(lambda ind: ind.fitness.values[0])
	stats.register("average", numpy.mean)
	stats.register("max", numpy.max)
	stats.register("min", numpy.min)


	current_datas = []
	training_range = len(training_set); sample_size = 0
	if (training_range >= 30):
		sample_size = 30
	else:
		sample_size = training_range

	training_index = random.sample(list(range(training_range)), sample_size)
	
	for index in training_index:
		current_datas.append(training_datas[index])
		print list(training_set)[index]
		print len(current_datas[-1])

	#if using gpu, copyt current_datas to gpu memory
	#get inidividual list and compile them beforehand
	if (bool(usegpu)):
		cudatool.mem_cp(current_datas)

		individuals = get_individual_list(population)
		cudatool.Compile(individuals)
	else:
		individuals = [str(indv) for indv in population]

	fits = toolbox.map(toolbox.evaluate, range(len(individuals)))
	for fit, ind in zip(fits, population):
		ind.fitness.values = fit

	current_bests = tools.HallOfFame(8)
	current_bests.update(population)

	print "init pop completed"

	for i in range(ngen):
		current_datas = []
		training_index = random.sample(list(range(training_range)), sample_size)
		#set current data
		for index in training_index:
			current_datas.append(training_datas[index])

		#if using gpu, copy current data to gpu
		if (bool(usegpu)):
			cudatool.mem_cp(current_datas)	
		
		#logging	
		print "current training data set for generation "
		tr_list = ""
		for index in training_index:
			 tr_list += "," +str(list(training_set)[index])
		print tr_list
		print "\n"

		offspring = algorithms.varAnd(population, toolbox, cxpb = 1.0, mutpb = 0.1)
		if (bool(usegpu)):
			individuals = get_individual_list(offspring + list(current_bests))
			cudatool.Compile(individuals)
		else:
			individuals = [str(indv) for indv in offspring + list(current_bests)]

		#apply toolbox.evaluate to every individuals in offspring --> set of fitness values is returned
		fits = toolbox.map(toolbox.evaluate, range(len(offspring)))
		for fit, ind in zip(fits, offspring):
			ind.fitness.values = fit

		#reevalute fitness of current population's bests using training set which is used for making offspring
		fits = toolbox.map(toolbox.evaluate, [len(offspring) + v for v in range(8)])
		for fit, ind in zip(fits, current_bests):
			ind.fitness.values = fit
		bests = list(current_bests)

		#select next population between offsrping and eight best individual
		population = toolbox.select(offspring + bests, len(population))
		#update current best for this new population
		current_bests.update(population)
	
		#Logging current status the population	
		stats_result = stats.compile(population)
		#print "=================Statistics================="
		print "\t\tGeneration " + str(i) + ": " + str(stats_result["max"]) + "(MAX), " + str(stats_result["average"]) + "(AVG), " + str(stats_result["min"]) + "(MIN)" 	
		
	return population

#Evolve and return the best individual after GP with 100 generation
#Use of seed is only required if random-tie-breaking is needed; if not MAX-tie breaking is default 
#and thus no need for seed(None)
def evolve(maxTreeDepth, minTreeDepth, initMaxTreeDepth, current_seed):
	global toolbox, seed
	#initialize seed for current GP 
	seed = current_seed
	
	print "in evolve"

	#Setting parameter(feature) for evolving: pset == feature set
	pset = gp.PrimitiveSet("main",40)
	pset.addPrimitive(gp_add, 2)
	pset.addPrimitive(gp_mul, 2)
	pset.addPrimitive(gp_sub, 2)
	pset.addPrimitive(gp_div, 2)
	pset.addPrimitive(gp_sqrt, 1)
	pset.addPrimitive(gp_neg, 1)
	pset.addTerminal(1.0)
	
	pset.renameArguments(ARG0 = "ochiai")
	pset.renameArguments(ARG1 = "jaccard")
	pset.renameArguments(ARG2 = "gp13")

	pset.renameArguments(ARG3 = "wong1")
	pset.renameArguments(ARG4 = "wong2")
	pset.renameArguments(ARG5 = "wong3")
	pset.renameArguments(ARG6 = "tarantula")
	pset.renameArguments(ARG7 = "ample")
	pset.renameArguments(ARG8 = "RussellRao")
	pset.renameArguments(ARG9 = "SorensenDice")
	pset.renameArguments(ARG10 = "Kulczynski1")
	pset.renameArguments(ARG11 = "SimpleMatching")
	pset.renameArguments(ARG12 = "M1")
	pset.renameArguments(ARG13 = "RogersTanimoto")
	pset.renameArguments(ARG14 = "Hamming")
	pset.renameArguments(ARG15 = "Ochiai2")
	pset.renameArguments(ARG16 = "Hamann")
	pset.renameArguments(ARG17 = "dice")
	pset.renameArguments(ARG18 = "Kulczynski2")
	pset.renameArguments(ARG19 = "Sokal")
	pset.renameArguments(ARG20 = "M2")
	pset.renameArguments(ARG21 = "Goodman")
	pset.renameArguments(ARG22 = "Euclid")
	pset.renameArguments(ARG23 = "Anderberg")
	pset.renameArguments(ARG24 = "Zoltar")
	pset.renameArguments(ARG25 = "ER1a")
	pset.renameArguments(ARG26 = "ER1b")
	pset.renameArguments(ARG27 = "ER5a")
	pset.renameArguments(ARG28 = "ER5b")
	pset.renameArguments(ARG29 = "ER5c")
	pset.renameArguments(ARG30 = "gp02")
	pset.renameArguments(ARG31 = "gp03")
	pset.renameArguments(ARG32 = "gp19")

	pset.renameArguments(ARG33 = "churn")
	pset.renameArguments(ARG34 = "max_age")
	pset.renameArguments(ARG35 = "min_age")
	pset.renameArguments(ARG36 = "num_args")
	pset.renameArguments(ARG37 = "num_vars")
	pset.renameArguments(ARG38 = "b_length")
	pset.renameArguments(ARG39 = "loc")

	creator.create("FitnessMin", base.Fitness, weights = (-1.0,))
	creator.create("Individual", gp.PrimitiveTree, fitness = creator.FitnessMin, pset = pset)
	
	toolbox = base.Toolbox()

	toolbox.register("expr", gp.genHalfAndHalf, pset = pset, min_ = minTreeDepth, max_ = initMaxTreeDepth)
	toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
	toolbox.register("population", tools.initRepeat, list, toolbox.individual, n = 2)

	toolbox.register("compile", gp.compile, pset = pset)
	toolbox.register("evaluate", eval_func)

	#selBest is for the elitism	
	toolbox.register("select", tools.selBest)
	toolbox.register("mate", gp.cxOnePoint)
	toolbox.register("mutate", gp.mutUniform, expr = toolbox.expr, pset = pset)

	toolbox.decorate("mate", gp.staticLimit(key = operator.attrgetter("height"), max_value = maxTreeDepth))
	toolbox.decorate("mutate", gp.staticLimit(key = operator.attrgetter("height"), max_value = maxTreeDepth))

	#Default setting is population = 40, generation = 100, best = 8
	#generate initial population	
	pop = toolbox.population(n=40)
	pop = gp_w_elit(pop, 100)
 
	#HallOfFame contains the best inidividual that ever lived in the population during the evolution
	best = tools.HallOfFame(8)
	#8 best
	best.update(pop)

#	Logging
#	print "\nThe best individual " 
#	print "\t\t expressin : " + str(best[0]) 
#	print "\t\t fitness   : " + str(best[0].fitness.values[0])	

	return best



if __name__ == "__main__":
	#Parse arguments from command line
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = "Final_data_w_norm")
	parser.add_argument("-dest", action = "store", default = "output")
	parser.add_argument("-resultid", action = "store", default = "0", help = "id of the result: result_id.result.csv")
	parser.add_argument("-pairFile", action = "store", default = "pair.txt", help = "a name of file where pairs of data files for cross-validation are written")
	parser.add_argument("-pairid", action = "store", default = None)
	parser.add_argument("-seed", action = "store", default = None)
	parser.add_argument("-maxTreeDepth", action = "store", default = 8, type = int, help = "maximum depth of the tree")
	parser.add_argument("-minTreeDepth", action = "store", default = 1, type = int, help = "minimum depth of the tree")
	parser.add_argument("-initMaxTreeDepth", action = "store", default = 6, type = int, help = "maximum depth of tree for the initialization: SHOULD BE SMALLER OR EQUAL TO 'maxTreeDepth'")
	parser.add_argument("-usegpu", action = "store", default = 0, type = int, help = "0 if not using gpu and 1 if using gpu")

	args = parser.parse_args()
	parser_check.argument_check(parser, ["pair id"], [args.pairid])
	usegpu = args.usegpu
	#if using gpu, then import cudaconv and generate instance of it(cudatool)
	if (bool(usegpu)):
		import cudaconv
		cudatool = cudaconv.CUDATool()

	t = time.time()
	print "start"
	print t
	#sys.argv[1] = dest of output directory, sys.argv[2] = result id of output, sys.argv[3] = pair id, sys.argv[4] = seed
	#experiment(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
	experiment(args.datadir, args.dest, args.resultid, args.pairFile, args.pairid, args.maxTreeDepth, args.minTreeDepth, args.initMaxTreeDepth, args.seed)
	
	t2 = time.time()
	print "end"
	print t2
	print "total: "
	print "{0} seconds".format(t2 -t)
