"""
a list of operators(method) used in gp for FLUCCS to formulate risk evaluation formula
"""
import math

def gp_add(a, b): return a+b

def gp_sub(a, b): return a-b

def gp_mul(a, b): return a*b

def gp_div(a, b): return 1 if b == 0 else a/b

def gp_sqrt(a): return math.sqrt(abs(a))

def gp_neg(a): return -a
