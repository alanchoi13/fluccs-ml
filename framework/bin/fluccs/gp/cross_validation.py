#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

import os

#Get file name for pair_file, contains information about the pair, and data directory
#Return data with ten cross validation structure
def get_10_cross_validation(pair_file, data_dir):
	pair_datas = dict()
	handler = open(pair_file, "r"); count = 0; key = ""
	while (True):
		line = handler.readline()
		if not line: break
		line.rstrip()
		data = line.split(",")
		if (count == 0):
			key = data[0].rstrip(); count += 1
		elif (count == 1):
			temp = []
			for e in data:
				line = e.rstrip()
				temp.append(os.path.join(data_dir, line))
			pair_datas[key] = [temp]; count += 1
		else:
			temp = []
			for e in data:
				line = e.rstrip()
				temp.append(os.path.join(data_dir, line))
			pair_datas[key].append(temp)
			count = 0

	handler.close()	
	return pair_datas
