#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
Prepare for cases which use cobertura as coverage tool
"""
import sys, os
import subprocess
import Mod_XML
import xml.etree.ElementTree as ET
import Prepare_Time

#modification for using cobertura
def modify_cobertura(datadir, filename, new_filename = None, _index = None):
	doc = ET.parse(filename)
	if (_index is None):
		_index = 0
	Mod_XML.add_node("import", add_attribs = [("file", "build_support.xml")], parent = doc.getroot(), index = _index)
	if (new_filename is None):
		doc.write(filename)
	else:
		doc.write(new_filename)

def modify_Math(datadir):
	doc = ET.parse("build.xml")
	(target_node, _) = Mod_XML.find_node(doc, "target",("name", "jar"), doc.getroot())
	target_node.attrib.pop("depends")
	doc.write("build.xml")
	

def modify_Time_1_11(datadir):
	doc = ET.parse("build.xml")
	(maven_importNode, _) = Mod_XML.find_node(doc, "import", ("file", "maven-build.xml"), doc.getroot())
	index = doc.getroot().getchildren().index(maven_importNode) + 1
	#Mod_XML.delete_node("import", ("file", "maven-build.xml"), doc = doc)
	Mod_XML.add_node("import", add_attribs = [("file", "build_support.xml")], parent = doc.getroot(), index = index)

	#(build_support_importNode, _) = Mod_XML.find_node(doc, "import", ("file", "build_support.xml"), doc.getroot())
	#print build_support_importNode
	#index = index + 1
	#Mod_XML.add_node("import", add_attribs = [("file", "mave-build.xml")], parent = doc.getroot(), index = 0)
	#print "add_node index: " + str(index)
	#Mod_XML.add_node("property", add_attribs = [("name", "source.home"), ("value", "${maven.build.srcDir.0}")], parent = doc.getroot(), index = index)
	#index = index + 1
	#Mod_XML.add_node("property", add_attribs = [("name", "test.home"), ("value", "${maven.build.testDir.0}")], parent = doc.getroot(), index = index)
	doc.write("build.xml")
	
	doc = ET.parse("build_support.xml")
	Mod_XML.add_node("path", doc = doc, node_name = "target", identifier = ("name", "fluccs.init"), add_attribs = [("id", "compile.classpath"), ("refid", "build.classpath")])
	Mod_XML.add_node("path", doc = doc, node_name = "target", identifier = ("name", "fluccs.init"), add_attribs = [("id", "test.classpath"), ("refid", "build.test.classpath")])

	Mod_XML.add_node("property", doc = doc, node_name = "target", identifier = ("name", "fluccs.init"), add_attribs = [("name", "source.home"), ("value", "${maven.build.srcDir.0}")])
	Mod_XML.add_node("property", doc = doc, node_name = "target", identifier = ("name", "fluccs.init"), add_attribs = [("name", "test.home"), ("value", "${maven.build.testDir.0}")])
	Mod_XML.add_node("pathelement", doc = doc, node_name = "path", identifier = ("id", "fluccs.test.classpath"), add_attribs = [("location", "${maven.build.outputDir}")])
	Mod_XML.add_node("pathelement", doc = doc, node_name = "path", identifier = ("id", "fluccs.test.classpath"), add_attribs = [("location", "${maven.build.testOutputDir}")])
	doc.write("build_support.xml")
	
	doc = ET.parse("maven-build.xml")
	(jarNode, _) = Mod_XML.find_node(doc, "target", ("name", "jar"), doc.getroot())
	jarNodeIndex = doc.getroot().getchildren().index(jarNode)
	doc.getroot().remove(jarNode)
	#Mod_XML.delete_node("jar", ("jarfile", "${maven.build.dir}/${maven.build.finalName}.jar"), doc = doc, parent = doc.getroot())	
	Mod_XML.add_node("target", add_attribs = [("name", "jar")], parent = doc.getroot(), index = jarNodeIndex)
	(newJarNode, _) = Mod_XML.find_node(doc, "target", ("name", "jar"), doc.getroot())
	Mod_XML.add_node("jar", add_attribs = [("basedir", "${maven.build.outputDir}"), ("jarfile", "${maven.build.dir}/${maven.build.finalName}.jar"), ("manifest", "src/conf/MANIFEST.MF")], parent = newJarNode)
	doc.write("maven-build.xml")

def checkValid(pid, bid):
	if (pid == "Lang"):
		if (bid >=1 and bid <= 65):	
			return True
	elif (pid == "Time"):
		if (bid >=1 and bid <= 27):
			return True
	elif (pid == "Math"):
		if (bid >= 1 and bid <= 106):
			return True
	elif (pid == "Closure"):
		if (bid >= 1 and bid <= 133):
			return True
	elif (pid == "Chart"):
		if (bid >= 1 and bid <= 26):
			return True
	elif (pid == "Mockito"):
		if (bid >= 1 and bid <= 38):
			return True
	else:
		print "Project " + pid + ": wrong bug id: " + str(bid)
		return False


def prepare(pid, bid, datadir):	
	current_dir = os.getcwd()
	#exec_dir means the directory of this file; datadir is the directory where the works should be taken on
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(datadir)
	if (checkValid(pid, bid)):
		try:
			shellfile = os.path.join(exec_dir, "prepare_w_cobertura.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError('Wrong return value')
		except ValueError as e:
			print ("shell script 'prepare_w_cobertura.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()	

		if (pid == "Time" and bid >=1 and bid <= 11):
			modify_Time_1_11(datadir)
		else:
			if (pid != "Chart"):
				if (pid != "Mockito" or os.path.exists(os.path.join(datadir, "build_support.xml"))):
					if (pid == "Math"): modify_Math(datadir)
					modify_cobertura(datadir, "build.xml")
	else:
		os.chdir(current_dir)
		sys.exit()
	
