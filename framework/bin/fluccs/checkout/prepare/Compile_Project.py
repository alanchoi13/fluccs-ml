#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

"""
execute appropriate shell file, which will compile both source and test codes and extract possible testcases, using given project id and bug id.
"""
import sys, os
import subprocess

current_dir = os.getcwd()
exec_dir = os.path.dirname(os.path.abspath(__file__))

def execute_compile(shellfile):
	global current_dir
	try:
		ret = subprocess.call([shellfile], shell = True)
		if (ret): raise ValueError("wrong return value")
	except ValueError as e:
		print ("compile shell script \"" + shellfile + "\" execution error", e)
		os.chdir(current_dir)
		sys.exit()

def compile_files(pid, bid, datadir, use_cobertura):
	global current_dir
	#move to datadir where the compilation should be done
	os.chdir(datadir)
	if (use_cobertura):
		shellfile = os.path.join(exec_dir, "use.cob.compile.tests.sh")
		execute_compile(shellfile)
	elif (pid == "Lang"):
		shellfile = os.path.join(exec_dir, "useJacoco/compile.tests.sh")
		execute_compile(shellfile)
	elif (pid == "Time"):
		if (bid <= 11):
			shellfile = os.path.join(exec_dir, "useJacoco/my.compile.tests.sh")
			execute_compile(shellfile)
		else:
			shellfile = os.path.join(exec_dir, "useJacoco/compile.tests.sh")
			execute_compile(shellfile)
	elif (pid == "Math"):
		if (bid <= 91 or bid == 99):
			shellfile = os.path.join(exec_dir, "useJacoco/compile.tests.sh")
			execute_compile(shellfile)
		else:
			shellfile = os.path.join(exec_dir, "useJacoco/my.compile.tests.sh")
			execute_compile(shellfile)
	elif (pid == "Closure"):
			shellfile = os.path.join(exec_dir, "useJacoco/compile-tests.sh")
			execute_compile(shellfile)
	elif (pid == "Chart"):
			shellfile = os.path.join(exec_dir, "useJacoco/compile-tests.sh")
			execute_compile(shellfile)
	elif (pid == "Mockito"):
			shellfile = os.path.join(exec_dir, "useJacoco/mockito-compile.tests.sh")
			execute_compile(shellfile)
	else:
		print "wrong project id " + pid
		os.chdir(current_dir); sys.exit()
	os.chdir(current_dir)
	
