#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
collection of xml modifying methods for project Lang, Time, Math, Closure.
"""
import sys,os
import xml.etree.ElementTree as ET

def rename_project(doc, new_name):
	root = doc.getroot()
	root.attrib["name"] = new_name

#Return jar node name and name of target attribute
def get_name(pid, bid):
	node_name = None; target_name = None; target_val = None
	if (pid == "Lang"):
		if (bid > 20 and bid < 42):
			node_name = "package"; target_name = "jarfile"; target_val = "Lang.jar"
		elif (bid >= 42 and bid <= 65):
			node_name = "jar"; target_name = "jarfile"; target_val = "Lang.jar"
		else:#no need for this method
			node_name = None
	elif (pid == "Math"):
		if (bid < 92 or bid == 99):
			node_name = "jar"; target_name = "jarfile"; target_val = "Math.jar"
		else:
			node_name = None
	elif (pid == "Time"):
		if (bid <= 11):#1-11
			node_name = "package"; target_name = "jarfile"; target_val = "Time.jar"
		else:#12-27: no need for this method
			node_name = None
	elif (pid == "Closure"):		
		node_name = "jar"; target_name = "destfile"; target_val = "Closure.jar"

	return (node_name, target_name, target_val)


#mod_attrib: dictionary of target attribute's name and its new value
def mod_attrib(mod_attrib, doc = None, node_name = None, identifier = None, target_node = None):
	if target_node is None:
		if (doc is not None and node_name is not None and identifier is not None):
			#for each identifier pair
			(target_node, parent_node) = find_node(doc, node_name, identifier, doc.getroot()) #i.e. find_node(doc, "target", {"name":"jar"/"package"})
			(attrib_val, val) = mod_attrib
			target_node.attrib[attrib_val] = val
		else:
			print "Missing argument in mod_attrib"
			sys.exit()
	else:
		(attrib_val, val) = mod_attrib
		target_node.attrib[attrib_val] = val

#Add new sub-element to target node(identified by value in identifier)	
#add_attribs is a dictionary which contains new element's attribute name and value
#def add_node(doc, node_name, identifier, new_node_name, add_attrib = None, index = None):
def add_node(new_node_name, doc = None, node_name = None, identifier = None, add_attribs = None, index = None, parent = None):
	if parent is None:#if parent is None, first find the parent using given identifier
		if (doc is not None and node_name is not None and identifier is not None):
			(parent, _) = find_node(doc, node_name, identifier, doc.getroot())#find parent node
	new_element = ET.Element(new_node_name)
	if (add_attribs is not None):
		for add_attrib in add_attribs:
			(attrib_val, val) = add_attrib
			new_element.attrib[attrib_val] = val
	
	if (index is None):
		parent.append(new_element)
	else:
		parent.insert(index, new_element)

#recursively find target node; return the first match
def find_node(doc, node_name, identifier, start_node):
	#print node_name
	nodes = start_node.findall(node_name)
	#print nodes
	(attrib_val, val) = identifier
	target_node = None; parent_node = start_node
	for node in nodes:
		if (attrib_val in node.attrib):
			if (node.attrib[attrib_val] == val):
				target_node = node; print 1; print target_node.tag; print target_node.attrib; break
	if (target_node is None):
		#Recursively find target node with setting current node as starting point
		if (not nodes):#empyt nodes []
			for node in list(start_node):#search all children
				(target_node, parent_node) = find_node(doc, node_name, identifier, node)
				if (target_node is not None):
					#print 2
					#print target_node.tag
					#print target_node.attrib
					break
		else:
			for node in nodes:
				(target_node, parent_node) = find_node(doc, node_name, identifier, node)
				if (target_node is not None):
					#print 3
					#print target_node.tag
					#print target_node.attrib
					break
	return (target_node, parent_node)	

#Delete node_name tag which matches with identifier
def delete_node(node_name, identifier, doc = None, parent = None, parent_node = None):
	if (parent is None and parent_node is None):#no given parent, find it
		(target_node, parent_node) = find_node(doc, node_name, identifier, doc.getroot())
		#parent_node.remove(target_node)
	else:#specific parent(direct above tag) is given
		if (parent_node is None):
			(parent_node, _) = find_node(doc, parent["node_name"], parent["identifier"], parent["parent"])
		(target_node, _) = find_node(doc, node_name, identifier, parent_node)
	parent_node.remove(target_node)

#delete a given attribute(delete_attrib)
#if target_node, which is assumed to have target attribute, is given, delete attribute from the node specified by identifier and node_name  
def delete_attrib(delete_attrib, doc = None, node_name = None, identifier = None, target_node = None):
	if target_node is None:
		if (doc is not None and node_name is not None and identifier is not None):
			(target_node, parent_node) = find_node(doc, node_name, identifier, doc.getroot())
			target_node.attrib.pop(delete_attrib)
		else:
			print "Missing argument in delete_attrib"
			sys.exit()
	else:
		target_node.attrib.pop(delete_attrib)

