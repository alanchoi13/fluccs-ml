#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
prepare for FLUCCs's operations.
include distribution of required files for each faulty version source code, compilation, retrieval of source code from 
the faulty version, extraction of testcases list, modification of xml files and etc. 
"""
import os
import argparse
import Prepare_Lang, Prepare_Math, Prepare_Time, Prepare_Closure, Prepare_Chart, Prepare_Mockito
import Prepare_CobBased
import Distribute
import Get_Source
import Compile_Project
import subprocess
import sys

parent_dir = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import parser_check
import Get_Revision

def prepare(pid, bid, datadir, use_cobertura):
	if (use_cobertura):
		Prepare_CobBased.prepare(args.pid, args.bid, args.datadir)
	elif (pid == "Lang"):
		Prepare_Lang.prepare(args.bid, args.datadir, args.use_cobertura)
	elif (pid == "Math"):
		Prepare_Math.prepare(args.bid, args.datadir)	
	elif (pid == "Time"):
		Prepare_Time.prepare(args.bid, args.datadir)
	elif (pid == "Closure"):
		Prepare_Closure.prepare(args.bid, args.datadir)
	elif (pid == "Chart"):
		Prepare_Chart.prepare(args.bid, args.datadir)
	elif (pid == "Mockito"):
		Prepare_Mockito.prepare(args.bid, args.datadir)
	else:
		print "Wrong project id " + pid
		sys.exit()


#main part
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-pid", action = "store", default = None)
	parser.add_argument("-bid", action = "store", default = None, type = int)
	parser.add_argument("-datadir", action = "store", default = ".")
	parser.add_argument("-use_cobertura", action = "store", default = "1", type = int);

	#Parse and Check argument
	args = parser.parse_args()

	parser_check.argument_check(parser, args_name_list = ["pid", "bid"], args_val_list = [args.pid, args.bid])
	current_dir = os.getcwd()
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	#Distribute tar file to each datadir; Distri_dir is parent directory of current directory
	Distri_dir = os.path.abspath(os.path.join(exec_dir, os.pardir))
	Distribute.distribute_tar(Distri_dir, args.datadir,args.pid, args.bid, args.use_cobertura)
	
	extract_class_file = os.path.join(exec_dir, "extract_testcases_dynamic.pl")
	retrieve_testclasses_file = os.path.join(exec_dir, "Retrieve_testclasses.py")
	cmd = "cp " + extract_class_file + " " + retrieve_testclasses_file + " " + args.datadir
	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(_, std_err) = proc.communicate()
	if (not (not std_err)): print std_err; sys.exit()

	prepare(args.pid, args.bid, args.datadir, args.use_cobertura)
	if (args.pid == "Time" and args.bid >= 22 and args.bid <=27):
		os.chdir(args.datadir)
		(rev_id, _) = Get_Revision.get_buggy_fix_revid(args.pid, args.bid)
		cmd = "git checkout " + rev_id + " JodaTime/src/main"
		proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
		(std_out, std_err) = proc.communicate()
		if (not (not std_err)): print "error in checkout buggy version source code: " + cmd; os.chdir(current_dir); sys.exit()
		cmd = "rm -Rf src/main"
		proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
		(std_out, std_err) = proc.communicate()
		if (not (not std_err)): print "error in moving test directory under JodaTime/src: " + cmd; os.chdir(current_dir); sys.exit()
		cmd = "mv JodaTime/src/main src/"
		proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
		(std_out, std_err) = proc.communicate()
		if (not (not std_err)): print "error in moving test directory under JodaTime/src: " + cmd; os.chdir(current_dir); sys.exit()
		os.chdir(current_dir)
	else:
		Get_Source.get_source(args.pid, args.bid, args.datadir)
	Compile_Project.compile_files(args.pid, args.bid, args.datadir, args.use_cobertura)
		
	#Prepare process	
#	if (args.use_cobertura):
#		Prepare_CobBased.prepare(args.pid, args.bid, args.datadir)
#		if (args.pid == "Time" and args.bid >= 22 and args.bid <=27):
#			os.chdir(args.datadir)
#			(rev_id, _) = Get_Revision.get_buggy_fix_revid(args.pid, args.bid)
#			cmd = "git checkout " + rev_id + " JodaTime/src/main"
#			proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
#			(std_out, std_err) = proc.communicate()
#			if (not (not std_err)): print "error in checkout buggy version source code"; os.chdir(current_dir); sys.exit()
#			cmd = "rm -Rf src/main"
#			proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
#			(std_out, std_err) = proc.communicate()
#			if (not (not std_err)): print "error in moving test directory under JodaTime/src"; os.chdir(current_dir); sys.exit()
#			cmd = "mv JodaTime/src/main src/"
#			(std_out, std_err) = proc.communicate()
#			if (not (not std_err)): print "error in moving test directory under JodaTime/src"; os.chdir(current_dir); sys.exit()
#			os.chdir(current_dir)	
#		else:
#			Get_Source.get_source(args.pid, args.bid, args.datadir)	
#		Compile_Project.compile_files(args.pid, args.bid, args.datadir, args.use_cobertura)
#	elif (args.pid == "Lang"):
#		Prepare_Lang.prepare(args.bid, args.datadir, args.use_cobertura)
#		Get_Source.get_source(args.pid, args.bid, args.datadir)
#		Compile_Project.compile_files(args.pid, args.bid, args.datadir, args.use_cobertura)
#	elif (args.pid == "Math"):
#		Prepare_Math.prepare(args.bid, args.datadir)
#		Get_Source.get_source(args.pid, args.bid, args.datadir)
#		Compile_Project.compile_files(args.pid, args.bid, args.datadir)
#	elif (args.pid == "Time"):
#		Prepare_Time.prepare(args.bid, args.datadir, args.use_cobertura)
#		print "in Time----------\n";
#		if (args.bid >= 22 and args.bid <=27):#Should move test directory under JodaTime 
#			print "in --------\n"
#			os.chdir(args.datadir)
#			(rev_id, _) = Get_Revision.get_buggy_fix_revid(args.pid, args.bid)
#			cmd = "git checkout " + rev_id + " JodaTime/src/main" if (use_cobertura) \
#						else "git checkout " + rev_id + " JodaTime/src/main"
#			proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
#			(std_out, std_err) = proc.communicate()
#			if (not (not std_err)): print "error in checkout buggy version source code"; os.chdir(current_dir); sys.exit()
#			cmd = "rm -Rf src/main" if (use_cobertura) else "rm -Rf JodaTime/src/test"
#			proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
#			(std_out, std_err) = proc.communicate()
#			if (not (not std_err)): print "error in moving test directory under JodaTime/src"; os.chdir(current_dir); sys.exit()
#			cmd = "mv JodaTime/src/main src/" if (use_cobertura) else "cp -Rf src/test JodaTime/src"
#			proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
#			(std_out, std_err) = proc.communicate()
#			if (not (not std_err)): print "error in moving test directory under JodaTime/src"; os.chdir(current_dir); sys.exit()
#			os.chdir(current_dir)
#		else:
#			Get_Source.get_source(args.pid, args.bid, args.datadir)
#		Compile_Project.compile_files(args.pid, args.bid, args.datadir)
#	elif (args.pid == "Closure"):
#		Prepare_Closure.prepare(args.bid, args.datadir)
#		Get_Source.get_source(args.pid, args.bid, args.datadir)
#		Compile_Project.compile_files(args.pid, args.bid, args.datadir)
#	elif (args.pid == "Chart"):#INITIAL VERSIONS ARE BASED ON SVN NOT GIT --> NEED TO MIGRATE THEM TO GIT
#		Prepare_Chart.prepare(args.bid, args.datadir)
#		print "prepare done"
#		Get_Source.get_source(args.pid, args.bid, args.datadir)
#		print "get source done"
#		Compile_Project.compile_files(args.pid, args.bid, args.datadir)
#	elif (args.pid == "Mockito"):
#		Prepare_Mockito.prepare(args.bid, args.datadir)
#		print "prepare done"
#		Get_Source.get_source(args.pid, args.bid, args.datadir)
#		print "get source done"
#		Compile_Project.compile_files(args.pid, args.bid, args.datadir)
#	else:
#		print "Wrong project id " + pid
#		sys.exit()
