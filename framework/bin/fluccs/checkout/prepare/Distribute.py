#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
distribute appropriate tar file using project id and bug id. 
"""
import sys, os
import shutil

#bug id(bid) and project id(pid) determine the tar file which contains required libraries, xml files and etc.
def get_tarfile(pid, bid, use_cobertura):
	tarfile = None
	if (use_cobertura):
		tarfile = "cobertura.tar"
	elif pid == "Lang":
		#if (use_cobertura): tarfile = "cobertura.tar"
		if (bid <= 20): tarfile = "Lang_1_20.tar"
		elif (bid >= 21 and bid <= 41): tarfile = "Lang_21_41.tar"
		elif (bid >= 42 and bid <= 44): tarfile = "Lang_42_44.tar"
		elif (bid >= 45 and bid <= 65): tarfile = "Lang_45_65.tar"
		else: print "Project Math: wrong bug id: " + str(bid); sys.exit()
	elif pid == "Math":
		if (bid <= 91 or bid == 99): tarfile = "Math_1_91_99.tar"
		elif (bid >= 92 and bid <= 98): tarfile = "Math_92_98.tar"
		elif (bid >= 100 and bid <= 106): tarfile = "Math_100_106.tar"
		else: print "Project Math: wrong bug id: " + str(bid); sys.exit()
	elif pid == "Time":
		if (bid <= 11): tarfile = "Time_1_11.tar"
		elif (bid >= 12 and bid <= 16): tarfile = "Time_12_16.tar"
		elif (bid >= 17 and bid <= 21): tarfile = "Time_17_21.tar"
		elif (bid >= 22 and bid <= 27): tarfile = "Time_22_27.tar"
		else: print "Project Math: wrong bug id: " + str(bid); sys.exit()
	elif pid == "Closure":
		#Now support for all 133 bugs
		if (bid <= 83 or (bid >= 107 and bid <= 133)): tarfile = "Closure_all_classpath.tar"
		elif (bid >= 84 and bid <= 106): tarfile = "Closure_classpath.tar"
		else: print "Project Closure: wrong bug id: " + str(bid); sys.exit()
	elif pid == "Chart":
		#print "Not yet"
		if (bid >= 1 and bid <= 26): tarfile = "Chart_1_26.tar"
		else: print "Project Chart: wrong bug id: " + str(bid); sys.exit()
	elif pid == "Mockito":
		if (bid >= 1 and bid <= 38): tarfile = "Mockito_1_38.tar"
		else: print "Project Chart: wrong bug id: " + str(bid); sys.exit()
	else:
		print "Wrong pid : " + pid; sys.exit()

	return tarfile

#distribute tarfile
def distribute_tar(root_dir, datadir, pid, bid, use_cobertura):
	tarfile = os.path.join(os.path.join(root_dir, pid), get_tarfile(pid, bid, use_cobertura))
	shutil.copy(tarfile, datadir)
	
