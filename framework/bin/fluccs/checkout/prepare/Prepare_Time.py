#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
project Joda Time specific preparation.
"""
import Mod_XML
import os, sys
import xml.etree.ElementTree as ET
import subprocess

def modify_XML_1_11(datadir, filename, bid):
	doc = ET.parse(filename)
	(jar_node_name, jarfile_id, new_jarfile_val) = Mod_XML.get_name("Time", bid)
	target_jar, _ = Mod_XML.find_node(doc, "target", ("name", jar_node_name), doc.getroot())
	jar_nodes = target_jar.findall("jar")
	jarfile_val = jar_nodes[0].attrib[jarfile_id]
	Mod_XML.add_node("copy", add_attribs = [("file", jarfile_val), ("tofile", new_jarfile_val)], parent = target_jar)
	#modify tag target (package)'s depends attribute to only contain compile
	Mod_XML.mod_attrib(("depends", "compile"), target_node = target_jar)
	
	doc.write(filename)


def prepare(bid, datadir):
	current_dir = os.getcwd()
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(datadir)
	if (bid <= 11):#1~11
		#XML process:modify maven-build.xml
		modify_XML_1_11(datadir,"maven-build.xml", bid)
		#Prepare
		try:
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_time_1_11.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError
		except ValueError as e:
			print("shell script 'prepare_time_1_11.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	elif (bid >= 12 and bid <= 16):#12~16
		try:	
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_time_12_16.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError
		except ValueError as e:
			print("shell script 'prepare_time_12_16.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	elif (bid >= 17 and bid <= 21):#17~21
		try:
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_time_17_21.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError
		except ValueError as e:
			print("shell script 'prepare_time_17_21.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	elif (bid >= 22 and bid <= 27):#22~27
		try:
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_time_22_27.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError
		except ValueError as e:
			print("shell script 'prepare_time_22_27.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	else:
		print "Project Time: wrong bug id: " + str(bid)
		os.chdir(current_dir)
		sys.exit()
	os.chdir(current_dir)
