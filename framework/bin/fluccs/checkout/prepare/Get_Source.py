#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
find out the name of source code directory and get the source code using git. 
"""
import subprocess
import re, os, sys

parent_dir = os.path.join(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import Get_Revision
import Migrate_SVN2GIT

def get_srcdir_name(datadir, cmd = None):
	if (cmd is None):
		cmd = "ant get-source-home"

	proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
	(std_out, std_err) = proc.communicate()
	if (not (not std_err)): 
		print "error in getting source directory name"
		print "Error message:\n\t%s" % (std_err)
		sys.exit()
	lines = std_out.rstrip().split("\n")
	dir_name = None
	for line in lines:
		if ("[echo] " in line):
			try:
				dir_name = re.search("\[echo\]\s*(.*)", line).group(1)
			except:
				print "find echo line but problem in extract dir name"; sys.exit()
			break
	if dir_name is None: print "failing to find source directory"; sys.exit()
	dir_name = dir_name.replace(datadir, "")
	if (dir_name.startswith('/')): dir_name = dir_name[1:]
	return dir_name

#retrieve the original buggy version source code
#if pid is Chart, which is controlled by svn, then extra works for retrieving sources from svn is required
def get_source(pid, bid, datadir):
	current_dir = os.getcwd()
	os.chdir(datadir)
	srcdir = get_srcdir_name(datadir)
	(bug_rev_id, _) = Get_Revision.get_buggy_fix_revid(pid, bid)
	if (pid == 'Chart'):
		(bug_rev_id, svn_dir) = Migrate_SVN2GIT.convert_svn_rev2git_rev(pid, bug_rev_id, datadir)
		cmd = "cp -Rf %s %s/git_ver" % (svn_dir, datadir)
		proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
		(std_out, std_err) = proc.communicate()
		print std_out
		if (not (not std_err) and proc.returncode != 0): 
			print "problem in executing " + cmd
			print "Error message:\n\t%s" % (std_err)
			print "Exit code is %d" % (proc.returncode)
			sys.exit()
		#_srcdir = srcdir.replace(datadir, "")
		#if (_srcdir.startswith('/')): _srcdir = _srcdir[1:]
		cmd = "cd %s/git_ver; git checkout %s; cp -Rf %s ../; cd ../; rm -Rf git_ver" % (datadir, bug_rev_id, srcdir)
		proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
		(std_out, std_err) = proc.communicate()
		if (not (not std_err) and proc.returncode != 0): 
			print "problem in executing " + cmd
			print "Error message:\n\t%s" % (std_err)
			print "Exit code is %d" % (proc.returncode)
			print proc.returncode
			sys.exit()	
	else:#for remaining projects(Lang, Math, Time, Closure, Mockito)
		cmd = "rm -Rf " + srcdir + "; git checkout " + bug_rev_id + " " + srcdir
		proc = subprocess.Popen(cmd.rstrip(), stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell = True)
		(std_out, std_err) = proc.communicate()
		if (not (not std_err) and proc.returncode != 0): 
			print "error in checkout buggy version source code: " + cmd
			print "Error message:\n\t%s" % (std_err)
			os.chdir(current_dir); sys.exit()	
		os.chdir(current_dir)	
