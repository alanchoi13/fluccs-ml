#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
project Mockito specific preparation.
"""

import sys, os
import xml.etree.ElementTree as ET
import Mod_XML
import subprocess

#add new 'import' node to the file(filename)
def modify_XML(datadir, filename, new_filename = None):
	doc = ET.parse(filename)
	Mod_XML.add_node("import", add_attribs = [("file", "build_support.xml")], parent = doc.getroot(), index = 0) 
	if (new_filename is None):
		doc.write(filename)
	else:
		doc.write(new_filename)

def prepare(bid, datadir):
	current_dir = os.getcwd()
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(datadir)
	if (bid >=1 and bid <= 38):
		build_already_exists = os.path.exists(os.path.join(datadir, "build.xml"))
		if (build_already_exists):
			modify_XML(datadir, "build.xml")
		try:
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_mockito.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError
		except ValueError as e:
			print ("shell script 'prepare_mockito.sh' execution error", e)
			sys.exit()
	else:
		print "Project Mockito: wrong bug id: " + str(bid)
		sys.exit()
