#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------

=pod

=head1 NAME
extract_testcases_dynamic.pl -- dynamically extract test classes and junit testcases of the each test class by executing each testcases.

=head1 SYSNOPSIS

=head1 In Embedding Script

extract junit test classes of the project and for the each test class, extract testcases. 

=cut

use strict;
use warnings;
use Try::Tiny;
use Cwd;
my $ARG_ERROR="wrong arguments";

my $base_dir = cwd();

test_classes($base_dir);

#extract testcases for all junit test classes
#results are saved in testclasses_list for the list of junit test classes and testcase_list_$testclass for testcases for each junit test class
#inputs are junit version and base directory
sub test_classes{
	my $base_dir = shift @_;
	my $predefined = -e "$base_dir/testsuites.txt";
#	my $predefined = undef;
	my $cmd = undef; my @all_tests = (); my $handler = undef;
	if ($predefined){#not used anymore
		$cmd = "python $base_dir/Retrieve_testclasses.py -datadir $base_dir -filename $base_dir/testsuites.txt -resultdir $base_dir";
		#print $cmd; `$cmd`;# logging
		open ($handler,"<","$base_dir/testclasses_list.txt");
		while (my $c=<$handler>){
			chomp $c; @all_tests = (@all_tests, $c);
		}
		close($handler);
	}
	else{ 
		$cmd="defects4j-fluccs 0 export -p tests.all -w $base_dir";
		print "$cmd\n";
		my $tests=`$cmd`;
		@all_tests=split /\n/,$tests;
	
		open ($handler,">","testclasses_list.txt");
		foreach my $x (@all_tests){
			if (!($x =~ m/^\s*$/) and !($x =~ m/d4j-export$/)){
				print $handler "$x\n";
			}
		}
		close($handler);
	}

	foreach my $test_class (@all_tests){
		extract_tc($test_class);
	}

	return;
}

#extract testcases for the argument class
#extracted results are saved in testcase_list.txt
sub extract_tc{
	@_ == 1 or die $ARG_ERROR;
	my $class= shift @_;
	my $cmd = undef;
	if ($class =~ m/\$/){
		my $outer = $`;
		my $inner = $';
		$cmd="ant dynamic.extract -Dclass=$outer -Dsub=$inner -Dsuballow=1";
	}
	else{
		$cmd="ant dynamic.extract -Dclass=$class -Dsuballow=0";
	}
	my $log=`$cmd 2>/dev/null`;
#	print "\n----------EXTRACT TESTCASES------------\n$log\n----------EXTRACT TESTCASES------------\n"; #logging
	return;
}

