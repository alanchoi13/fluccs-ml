#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
project Commons Lang specific preparation.
"""
import Mod_XML
import os, sys
import xml.etree.ElementTree as ET
import subprocess


def modify_XML_21_41(datadir, filename, bid):
	doc = ET.parse(filename)
	(jar_node_name, jarfile_id, new_jarfile_val) = Mod_XML.get_name("Lang", bid)
	target_jar, _ = Mod_XML.find_node(doc, "target", ("name", jar_node_name), doc.getroot())
	jar_nodes = target_jar.findall("jar")
	#copy jar to new jarfile and add this task under target_jar
	jarfile_val = jar_nodes[0].attrib[jarfile_id]
	Mod_XML.add_node("copy", add_attribs = [("file", jarfile_val), ("tofile", new_jarfile_val)], parent = target_jar)
	#delete manifest attributes from tag jar(under target (jar_node_name))
	Mod_XML.delete_attrib("manifest", target_node = jar_nodes[0])
	#modify tag target (jar)'s depends attribute to only contain compile
	Mod_XML.mod_attrib(("depends", "compile"), target_node = target_jar)
	#delete sub element manifest from tag target (jar)
	manifeset_node = jar_nodes[0].find("manifest")
	jar_nodes[0].remove(manifeset_node)

	ET.dump(doc)
	doc.write(filename)

	
def modify_XML_42_65(datadir, filename, bid):
	doc = ET.parse(filename)
	(jar_node_name, jarfile_id, new_jarfile_val) = Mod_XML.get_name("Lang", bid)
	target_jar, _ = Mod_XML.find_node(doc, "target", ("name",jar_node_name), doc.getroot())
	jar_nodes = target_jar.findall("jar")
	jarfile_val = jar_nodes[0].attrib[jarfile_id]
	Mod_XML.add_node("copy", add_attribs = [("file", jarfile_val), ("tofile", new_jarfile_val)], parent = target_jar)
	Mod_XML.add_node("import", add_attribs = [("file", "build_for_coverage.xml")], parent = doc.getroot(), index = 0)
	
	doc.write(filename)


def prepare(bid, datadir, use_cobertura = 0):
	#exec_dir means the directory of this file; datadir is the directory where the works should be taken on
	current_dir = os.getcwd()
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(datadir)
	if (bid <= 20):
		try:
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_lang_1_20.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError('wrong return value')
		except ValueError as e:
			print ("shell script 'prepare_lang_1_20.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()

	elif (bid >= 21 and bid <= 41):
		#XML process: modify maven-build.xml
		modify_XML_21_41(datadir, "maven-build.xml", bid)
		#Prepare
		try:
			shellfile = os.path.join(exec_dir, "useJacoco/prepare_lang_21_41.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError
		except ValueError as e:
			print ("shell script 'prepare_lang_21_41.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
		
	elif (bid >= 42 and bid <= 65):
		#XML Process: import build_for_coverage to build.xml and also modify build.xml
		modify_XML_42_65(datadir, "build.xml", bid)
		#Prepare
		try:
			if (bid <= 44):
				shellfile = os.path.join(exec_dir, "useJacoco/prepare_lang_42_44.sh")
			else:
				shellfile = os.path.join(exec_dir, "prepare_lang_45_65.sh")
			ret = subprocess.call([shellfile], shell = True)
			if (ret): raise ValueError
		except ValueError as e:
			print ("shell script 'prepare_lang_42_65.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	else:
		print "Project Lang: wrong bug id: " + str(bid)
		os.chdir(current_dir)
		sys.exit()
	os.chdir(current_dir)
