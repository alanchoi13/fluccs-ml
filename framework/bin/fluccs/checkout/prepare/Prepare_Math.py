#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
project Commons Math specific preparation.
"""
import Mod_XML
import sys, os
import xml.etree.ElementTree as ET
import subprocess

def modify_XML_1_91_99(datadir, filename, bid):
	doc = ET.parse(filename)
	Mod_XML.rename_project(doc, "Support Commons Math")	
	#modify jarfile name
	(jar_node_name, jarfile_id, new_jarfile_val) = Mod_XML.get_name("Math", bid)
	target_jar ,_ = Mod_XML.find_node(doc, "target", ("name", jar_node_name), doc.getroot())
	jar_nodes = target_jar.findall("jar")
	#copy jar to new jarfile and add this task under target_jar
	jarfile_val = jar_nodes[0].attrib[jarfile_id]
	Mod_XML.add_node("copy", add_attribs = [("file", jarfile_val), ("tofile", new_jarfile_val)], parent = target_jar)
	#add new element to test.classpath
	Mod_XML.add_node("pathelement", doc = doc, node_name = "path", identifier = ("id", "test.classpath"), add_attribs = [("location", "${basedir}/lib/junit-${junit.version}.jar")])
	Mod_XML.add_node("pathelement", doc = doc, node_name = "path", identifier = ("id", "test.classpath"), add_attribs = [("location", "${basedir}/lib/hamcrest.jar")])
	#delete path downloaded.lib.classpath and pathelement ${junit.jar} in test.classpath
	path_node = {"node_name":"path", "identifier":("id","test.classpath"), "parent":doc.getroot()}
	Mod_XML.delete_node("path", ("refid", "downloaded.lib.classpath"), doc = doc, parent = path_node)
	Mod_XML.delete_node("pathelement", ("location", "${junit.jar}"), doc = doc, parent = path_node)
	#delete useless property value: junit.home and junit.jar
	Mod_XML.delete_node("property", ("name", "junit.home"), doc = doc)
	Mod_XML.delete_node("property", ("name", "junit.jar"), doc = doc)
	#delete unwanted attribute from taet jar nod
	Mod_XML.delete_attrib("depends", doc = doc, node_name = "target", identifier = ("name", "jar"))
	#modify property junit.version value
	Mod_XML.mod_attrib(("value","4.12"), doc = doc, node_name = "property", identifier = ("name","junit.version"))
	#reflect the previous changes
	doc.write(filename)

def prepare(bid, datadir):
	current_dir = os.getcwd()
	exec_dir = os.path.dirname(os.path.abspath(__file__))
	os.chdir(datadir)
	if (bid <= 91 or bid == 99):
		#XML process
		modify_XML_1_91_99(datadir, "build.xml", bid)
		#Prepare
		try:
			shellfile = os.path.join(exec_dir, 'useJacoco/prepare_math_1_91_99.sh')
			ret = subprocess.call([shellfile], shell = True)#ret should be 0
			if (ret): raise ValueError('wrong return value')
		except ValueError as e:
			print("shell script 'prepare_math_1_91_99.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	elif (bid >= 92 and bid <= 98):
		try:
			shellfile = os.path.join(exec_dir, 'useJacoco/prepare_math_92_98.sh')
			ret = subprocess.call([shellfile], shell = True)#ret should be 0
			if (ret): raise ValueError('wrong return value')
		except ValueError as e:
			print("shell script 'prepare_math_92_98.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	elif (bid >= 100 and bid <= 106):
		try:
			shellfile = os.path.join(exec_dir, 'useJacoco/prepare_math_100_106.sh')
			ret = subprocess.call([shellfile], shell = True)#ret should be 0
			if (ret): raise ValueError('wrong return value')
		except ValueError as e:
			print("shell script 'prepare_math_100_106.sh' execution error", e)
			os.chdir(current_dir)
			sys.exit()
	else:
		print "Project Math: wrong bug id: " + str(bid)
		os.chdir(current_dir)
		sys.exit()
	os.chdir(current_dir)
