#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
gather data from datadir and generate intermediate result, method_all.csv.
"""
import sys
import os
import csv
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import parser_check

#retrieve data from datadir/filename
def get_datas(datadir, filename):
	handler = open(os.path.join(datadir, filename))
	csvReader = csv.reader(handler, delimiter = ",")
	temp = list(tuple(row) for row in csvReader)
	handler.close()
	
	datas = dict()
	for t in temp:
		datas[t[0]] = t[1:]
	return datas

#This is the one executed : add #of args, #of local var, instr_length, LOC
def write_method_all(covgDatadir, agechurnDatadir, compDatadir, resultdir):
	handler = open(os.path.join(resultdir, "method_all.csv"), "w")

	#retrieve necessary target metrics' data
	complexity_infos = get_datas(covgDatadir, "method_complexity.csv")
	ageAndchurn_infos = get_datas(agechurnDatadir, "methodAgeAndChurns.csv")
	spectra_infos = get_datas(compDatadir, "method_spectra.csv")

	for key in list(ageAndchurn_infos.keys()):
		all_data = key
		all_data += "," + ",".join(spectra_infos[key])
		#all_data += "," + ",".join(ageAndchurn_infos[key])
		#only use nomralized values(last three elements )for age and churn metrics
		all_data += "," + ",".join(ageAndchurn_infos[key][3:])
		all_data += "," + ",".join(complexity_infos[key])

		handler.write(all_data + "\n")
	handler.close()


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-covgDatadir", action = "store", default = None, help = "a directory where method level coverage metric files are stored")
	parser.add_argument("-agechurnDatadir", action = "store", default = None, help = "a directory where method level age and churn metric files are stored")
	parser.add_argument("-compDatadir", action = "store", default = None, help = "a directory where method level complexity metric files are stored")
	parser.add_argument("-resultdir", action = "store", default = None, help = "a directory where integrated method level metric files(method_all.csv) will be stored")

	args = parser.parse_args()

	parser_check.argument_check(parser, args_name_list = ["covgDatadir", "agechurnDatadir", "compDatadir", "resultdir"], args_val_list = [args.covgDatadir, args.agechurnDatadir, args.compDatadir, args.resultdir])
	write_method_all(args.covgDatadir, args.agechurnDatadir, args.compDatadir, args.resultdir)




