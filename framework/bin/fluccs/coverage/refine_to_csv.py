#--------------------------------------------------------------------------------
#Copyright (c) 2017 Jeongju Sohn and Shin Yoo
#
#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in
#the Software without restriction, including without limitation the rights to
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
#the Software, and to permit persons to whom the Software is furnished to do so,
#subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
#FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
#IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
#CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#--------------------------------------------------------------------------------
"""
extract data that is directly relevant to spectra from coverage result(spectra + extra).
"""
import sys, os
import csv
import argparse

parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir)
sys.path.insert(0, os.path.join(parent_dir, 'python'))
import parser_check

def _extract(datadir, target):
	handler = open(os.path.join(datadir, target), "r")
	datas = list()
	while True:
		line = handler.readline()
		if not line: break
		data = line.split("\':::\'")
		datas.append(data)
	datas.sort()
	handler.close()

	handler = open(os.path.join(datadir, "spectra.all.csv"), "w")
	for data in datas:
		line = ",".join([data[0][1:], data[1], data[4], data[5], data[6], data[7]])
		handler.write(line + "\n")
	handler.close()

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-datadir", action = "store", default = None)
	parser.add_argument("-target", action = "store", default = None)

	args = parser.parse_args()
	parser_check.argument_check(parser, args_name_list = ["datadir", "target"], args_val_list = [args.datadir, args.target])
	_extract(args.datadir, args.target)
